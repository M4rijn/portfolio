<?php
/**
 * Created by PhpStorm.
 * User: Marijn
 * Date: 30/04/2016
 * Time: 18:26
 */

$highlightedCases = [

    ////////////////////////////////////////////////////////////// PRESSURE COOKER

    [
        "title" => "Allescænner",
        "background" => "../school/s6/allescaenner/allescaenner.png",
        "content" => [
            "intro" => "Bij Maerschalk heb ik samen met mijn groepsgenoten een opdracht van New Brand Activators uitgewerkt. De opdracht luidde “verzin een innovatieve manier om de instore marketing van de EMTÉ te verbeteren en meetbaar te maken. Denk hierbij ook aan hoe de data wordt weergegeven”. Tijdens het uitwerken van deze opdracht heb ik mij vooral gericht op het onderdeel research, concepting, design, user experience, en de technische uitwerking.",
            "chapters" => [
                "Onze oplossing" => [
                    "text" => [
                        "Onze oplossing voor deze probleemstelling is de Allescænner. Dit product is een doorontwikkeling van de zelfscanner die EMTÉ al gebruikt. Er is een beeldscherm ingebouwd in de winkelwagen die in contact staat met de zelfscanner. De winkelwagen kan behalve met het bekende 50 cent-muntje ook met de Fijnproeverspas worden ontgrendeld. Zo weten we wie de winkelkar gebruikt. Op het beeldscherm worden relevante informatie en aanbiedingen getoond voor de betreffende klant, maar ook aan de hand van een gescand product. Dit werkt een op een zelfde manier als veel webshops cross selling inzetten. Omdat je weet welke advertentie aan welke klant wordt getoond weet je of bepaalde aanbiedingen goed werken of niet.",
                        "Buiten dit hoofdonderdeel zijn nog enkele andere features toegevoegd aan Allesænner. Zo wordt het winkellijstje dat je thuis hebt aangemaakt op het beeldscherm getoond en krijg je waarschuwingen als bepaalde stoffen waar je allergisch voor bent in het zojuist gescande product zitten."
                    ],
                    "right" => [
                        ["image", "../school/s6/allescaenner/laatste_gescande_product.png"]
                    ]
                ],
                "Rubrics: Concept" => [
                    "text" => [
                        "De vraag die werd gesteld door New Brand Activators was om offline media in de winkel meetbaar te maken. We hebben als groep gekozen om niet de huidige media (posters, vloerstickers, etc.) trackbaar te maken, maar om een nieuwe manier van instore marketing te ontwikkelen. Door gebruik te maken van een scherm in de winkelkar die is gekoppeld  aan de zelfscanner van EMTÉ kunnen relevante aanbiedingen worden getoond op het scherm. Bovendien kan de klant de winkelkar ontgrendelen met zijn of haar Fijnproeverspas. Dit houdt in dat ook persoonlijke aanbiedingen kunnen worden getoond.",
                        "In de vlogs kan worden gezien hoe we op het concept zijn gekomen.",
                        "Er kan makkelijk worden bijgehouden welke klant bepaalde aanbiedingen te zien krijgt. Daarnaast kun je ook meten of vervolgens het aangeboden product later wordt gescand. Je kunt dus bekijken welke aanbiedingen bij welke producten het beste werken. Met deze informatie kun je itereren om zo tot beter resultaten te komen."
                    ],
                    "right" => [
                        ["image", "../school/s6/allescaenner/10-concepten.jpg"]
                    ]
                ],
                "Rubrics: Design" => [
                    "text" => [
                        "Het design heb ik bijna volledig op me genomen. De interface van het scherm heb ik vormgegeven volgens de designguidelines van EMTÉ. De winkelketen is vrij recent overgestapt naar een volledig nieuwe huisstijl. Hier heb ik dankbaar gebruik van gemaakt, omdat de oude huisstijl niet van deze tijd is.",
                    ],
                    "right" => [
                        ["image", "../school/s6/allescaenner/laatste_gescande_product.png"]
                    ]
                ],
                "Rubrics: User Experience" => [
                    "text" => [
                        "[Hassenzahl]",
                    ],
                    "right" => [
                        ["", ""]
                    ]
                ],
                "Rubrics: Gebruikersonderzoek" => [
                    "text" => [
                        "Nadat een eerste prototype is gemaakt zijn we met de hele groep gaan testen in de EMTÉ. We hebben willekeurige klanten het prototype uit laten proberen en gevraagd wat zij ervan vonden. Wat voor ons belangrijk was, was of de ondervraagde klanten meerwaarde zagen in het concept. Vanaf het begin vonden we het belangrijk dat de getoonde aanbiedingen niet te opdringerig moesten worden. Daarom heb ik hier ook specifiek naar gevraagd tijdens de interviews. Uit de usertests bleek dat veel klanten de relevante aanbiedingen juist handig vonden, en niet opdringerig zoals door ons gevreesd.",
                    ],
                    "right" => [
                        ["", ""]
                    ]
                ],
                "Rubrics: Niveau product" => [
                    "text" => [
                        "Het prototype dat gebouwd is voor dit project is vrij ver uitgewerkt. Met een telefoon kan een barcode worden gescand (voor dit prototype ter vervanging van de zelfscanner van EMTÉ). De gegevens van de barcode worden via websockets naar de hoofdwebsite gestuurd. Zo kan zonder opnieuw pagina’s laden telkens een nieuw product worden gescand. Met Javascript wordt gezorgd dat het juiste product weergegeven op het scherm in de winkelkar als een product met de telefoon wordt gescand. Daarnaast zijn de andere features zoals een interactieve boodschappenlijst en zoekfunctie ook werkend gemaakt.",
                    ],
                    "right" => [
                        ["", ""]
                    ]
                ]
            ],
        ],
        "externallink" => [
            "url" => "",
            "urltext" => ""
        ]
    ],

    [
        "title" => "Agnes",
        "background" => "../school/s6/agnes/volkswagen.png",
        "description" => "",
        "content" => [
            "intro" => "Bij New Brand Activators hebben wij de opdracht gekregen om een campagne van 4 jaar te ontwikkelen. De opdracht kwam via Greenhouse group van Volkswagen (fictief) en het doel van de campagne is om meer mensen in zelfrijdende Volkswagens te krijgen die over 4 jaar op de markt komen.",
            "chapters" => [
                "Onze oplossing" => [
                    "text" => [
                        "De campagne die wij op hebben gezet begint met een hoax. Veel autofabrikanten maken al gebruik van apps om de status van jouw auto naar je telefoon te sturen. Wij vragen bekende vloggers uit de technologie- en autobranche om te vertellen dat zij steeds meer creepy berichten van hun huidige auto’s ontvangen via deze apps. De berichten worden in een anderhalf jaar opgebouwd van “de benzine is bijna leeg” naar “Ik wil nu een rondje gaan rijden!”. Hierdoor wordt aan de doelgroep het idee van een AI met een bewustzijn geloofwaardig."
                    ],
                    "right" => [
                        ["", ""]
                    ]
                ],
                "Rubrics: Trends" => [
                    "text" => [
                        "Deze ronde heb ik het trendonderzoek op mij genomen. Door een trendonderzoek te doen leg je op een overzichtelijke manier de relevante trends vast waardoor je weet waar je rekening mee moet houden waar je op in kunt spelen.",
                        "Deze opdracht is gericht op het delen van een zelfrijdende auto die over vier jaar uit komt. De campagne moet echter nu al starten. Er moet dus gekeken worden naar huidige trends en trends in de toekomst. Als naar de doelgroep wordt gekeken valt op dat deze steeds minder naar traditionele media zoals TV en Radio kijkt en luistert. Zij bevinden zich bijna alleen maar op social media. Als naar technieken wordt gekeken valt op dat zelfrijdende auto’s en elektrische auto’s de laatste tijd flink in opkomst zijn. Daarnaast valt op dat kunstmatige intelligentie steeds complexer wordt. Met deze bevindingen kan vervolgens naar een concept toe worden gewerkt.",
                        "Ik heb voor dit trendonderzoek gebruik gemaakt van een consumer trend canvas. Hierdoor wordt duidelijk wat de behoeften van de doelgroep zijn, waar de trend vanaf komt en waar het heen gaat."
                    ],
                    "right" => [
                        ["image", "../school/s6/agnes/consumer-trend-canvas.jpg"]
                    ]
                ],
                "Rubrics: Concept" => [
                    "text" => [
                        "Bla"
                    ],
                    "right" => [
                        ["", ""]
                    ]
                ],
                "Rubrics: Marketing" => [
                    "text" => [
                        "De briefing voor dit project was om een marketingcampagne te ontwikkelen die vier jaar duurt. De doelgroep die werd gegeven was erg breed: 10 tot 99-jarigen. Gelukkig werd hierin een focus op young professionals gelegd. Young professionals zijn mensen die van 20 tot 35 jaar oud en hebben een kantoorbaan. Uit dit doelgroeponderzoek blijkt dat een groot deel van deze groep naar het werk of school gaat met het OV. Daarom is ervoor gekozen om een deel van de campagne op het OV te richten.",
                        "De doelgroep bevindt zich ook vooral op social media en minder op traditionele media. Om deze reden heb ik er samen met mijn groep voor gekozen om de kick-off van de campagne te doen met hulp van bekende vloggers uit de auto- en tech branche. Deze personen hebben veel impact op onze doelgroep.",
                        "Door te richten op deze bevindingen hebben we de campagne een vorm kunnen geven die logisch is opgebouwd."
                    ],
                    "right" => [
                        ["", ""]
                    ]
                ],
                "Rubrics: Communicatie" => [
                    "text" => [
                        "De pitch voor dit project heb ik samen met Selime gepresenteerd. Ik heb ook de opbouw en vormgeving van de slides op mij genomen. We hebben elk onze eigen tekst geschreven en daarna elkaar feedback gegeven daarover. Zo zijn we op teksten gekomen die het concept goed uitleggen. Nadat de teksten waren bepaald hebben we de laatste twee dagen meerdere keren per dag geoefend zodat we de teksten uit ons hoofd zouden kennen met de eindpresentatie. Daarnaast hebben we de presentatie ook twee keer bij New Brand Activators gedaan voor verschillende collega’s zodat ook zij nog feedback konden geven.",
                        "Uiteindelijk met de eindpresentatie is alles goed gegaan. Ik kwam een paar keer niet goed uit mijn woorden, maar verder ben ik wel tevreden."
                    ],
                    "right" => [
                        ["youtube", "<iframe src=\"https://www.youtube.com/embed/HY-w6v4jctY\" frameborder=\"0\" allowfullscreen></iframe>"]
                    ]
                ]
            ]
        ],
        "externallink" => [
            "url" => "",
            "urltext" => ""
        ]
    ],

    ////////////////////////////////////////////////////////////////////////////////////////// S4

    [
        "title" => "Scrum",
        "background" => "../school/s4/scrum_intro.jpg",
        "description" => "",
        "content" => [
            "text" =>
                "<p>
                    <span class='bold'>
                        Theoretische kennis van Scrum
                    </span><br>
                        Scrum is een manier van werken die valt binnen het Agile framework. Door deze werkwijze toe te passen worden projecten in teamverband op een flexibele en effectieve manier gemaakt.
Dit is omdat er tijdens elke zogenaamde ‘sprint’ een geheel traject van concepting tot en met een potentieel ‘af’ product wordt doorlopen. Zo wordt er voor gezorgd dat er na elke sprint een nieuwe richting kan worden ingeslagen. Hoelang een sprint duurt kan per project verschillen.
De veelgebruikte tegenhanger van Scrum is de ‘waterval-methode’, waarbij van begin tot eind van een project aan één planning wordt vastgehouden. Een nadeel van deze methode is dat als er op het einde van een project een andere richting op wil worden gegaan dit haast onmogelijk is. Er kan niet tegen de stroom in terug worden gezwommen.<br>
                    <br>
                    <span class='bold'>
                        Voordelen van Scrum:
                    </span><br>
                    <ul>
                        <li><p>De effectiviteit van het team wordt verhoogd omdat iedereen in één oogopslag ziet waar iedereen mee bezig is en waar hij of zij zich mee bezig moet houden.</p></li>
                        <li><p>De Return Of Investment is maximaal omdat er geen tijd wordt verspild met het terug omhoog zwemmen tegen de waterval in.</p></li>
                        <li><p>Deze werkwijze zorgt ervoor dat je na elke sprint een af product kunt opleveren.</p></li>
                        <li><p>Enkel het nodige voor het project wordt gemaakt.</p></li>
                        <li><p>De voortgang in het project is makkelijk bij te houden.</p></li>
                    </ul>
                    <br>
                    <span class='bold'>
                        Nadelen van Scrum:
                    </span><br>
                    <ul>
                        <li><p>Het kan lastig zijn om meteen een af product te realiseren na de eerste sprint.</p></li>
                        <li><p>Het neemt veel tijd in beslag. Tijd die wordt gebruikt voor een 'daily standup' kan worden gebruikt voor het werken aan het project zelf.</p></li>
                        <li><p>Het wordt het centrale punt van alle zaken.</p></li>
                    </ul>
                    <br>
                    <span class='bold'>
                        Toepassing van Scrum in de proftaak
                    </span><br>
                    In S4 is gebruik gemaakt van de Scrum-methode om een digitale campagne voor Het Gelders Orkest te realiseren. De periode is opgedeeld in een viertal sprints, elk 3 á 4 weken lang. In deze proftaak is gebruik gemaakt van een Scrum-bord opgedeeld in de volgende categorieën:
                    <ul>
                        <li>Backlog</li>
                        <li>To do</li>
                        <li>Doing</li>
                        <li>Testing</li>
                        <li>Done</li>
                    </ul>
                    De backlog bestaat uit user-stories die we gebruiken om tijdens het project op terug te vallen. De To-Do-lijst bestaat uit taken die nog moeten worden gedaan. Onder ‘Doing’ staat per lid van de groep aangegeven wie aan wat bezig is. Als iemand hier mee klaar is wordt het overgeplaatst naar ‘Testing’, en als de andere groepsleden er ook tevreden over zijn is het onderdeel af. De taken die iedereen te doen had staan op post-its zodat ze makkelijk te verplaatsen zijn.<br>
                    <br>
                    Om te bepalen en bij te houden wie met wat bezig is en gaat zijn– vooral vanaf de laatste paar weken – ‘daily Scrums’ gehouden. Hierin werd formeel besproken wat de voortgang van het project was en wat er nog diende te gebeuren.<br>
                    <br>
                    <span class='bold'>
                        Actieve bijdrage aan het Scrum-proces
                    </span><br>
                    Ik had al eerder met de Scrum-methode gewerkt. Mede daarom ben ik gedurende het semester vaker bezig geweest met het Scrumbord. Ik heb me vaak met de takenverdeling en planning aan het begin van sprints bemoeid.<br>
                    <br>
                    <span class='bold'>
                        Tips
                    </span><br>
                    Tijdens dit project hadden we meer aandacht moeten besteden aan retrospectives aan het einde van elke sprint. Als we dit hadden gedaan was voor iedereen formeel duidelijk wat niet en wat wel goed ging tijdens de vorige sprint. Nu bespraken we het wel informeel met elkaar, maar hierdoor ging sommige communicatie langs elkaar heen en wist niet iedereen van iedereen wat hij ervan vond en wat de planning was voor de volgende sprint.<br>
                    <br>
                    Tijdens de eerste twee sprints van het project zijn we niet serieus omgegaan met de daily stand-ups. We hielden ze wel, maar bijna nooit was iedereen aanwezig wat de communicatie niet ten goede kwam.
                </p>",
            "right" => [
                ["image", "../school/s4/scrum2.jpg"],
                ["youtube", "<iframe src=\"https://www.youtube.com/embed/TePtgLJENmE?rel=0\" frameborder=\"0\" allowfullscreen></iframe>"]
            ]
        ],
        "externallink" => [
            "url" => "",
            "urltext" => ""
        ]
    ],
    [
        "title" => "Strategie &amp; concept",
        "background" => "../school/s4/sco_intro.jpg",
        "description" => "",
        "content" => [
            "text" =>
                "<p><span class='bold'>
                        Conceptueel denken
                    </span><br>
                    Aan het begin van het semester zijn verschillende concepten bedacht. Om op deze concepten te komen is gebruik gemaakt van verschillende soorten technieken om mee te brainstormen.<br>
                    <br>
                    We zijn begonnen met het bedenken van 200 ideeën. Het maakte niet uit hoe haalbaar of onhaalbaar deze ideeën waren. Doordat er zoveel willekeurige dingen worden opgeschreven kom je uiteindelijk op ideeën waar je eerst nooit aan had gedacht.<br>
                    <br>
                    Het bedenken van deze 200 ideeën is gedaan met verschillende brainstormtechnieken die te vinden zijn op: <a href='http://www.creatiefdenken.com/brainstormtechnieken.php' target='_blank'>http://www.creatiefdenken.com/brainstormtechnieken.php</a><br>
                    <br>
                    Er is gebruik gemaakt van ‘negatief brainstormen’, waarbij het doel is om zo slecht mogelijke ideeën te verzinnen. Deze slechte ideeën kun je vervolgens omdraaien naar goede ideeën. Ook is de techniek ‘mindmappen’ gebruikt waarbij een idee wordt verzonnen en daarna op wordt verder gedacht, waarna daar weer op wordt doorgedacht, etc. Als laatste hebben we gebruik gemaakt van ‘geforceerd combineren’. Bij deze techniek worden twee willekeurige woorden gepakt en geprobeerd te linken. Door dit te doen kunnen nieuwe invalshoeken worden gevonden.<br>
                    <br>
                    Het resultaat van 3 weken brainstormen was 5 concepten. Deze concepten zijn uitgewerkt met de “Slapend, ontwakend, levend”-methode. Door gebruik te maken van deze methode wordt meteen duidelijk wat het waarom en bestaansrecht van een concept zijn (slapend), wat de pay-off en/of proposities zijn (ontwakend) en hoe het concept wordt omgezet in producten of diensten (levend). Deze concepten waren:
                    <ul>
                        <li><a href='files/concept-interaction.pdf' target='_blank'>Interaction:</a> Het publiek heeft invloed op het concert waardoor het altijd een unieke ervaring is.</li>
                        <li><a href='files/concept-immersion.pdf' target='_blank'>Immersion:</a> De bezoeker wordt met alle zintuigen geprikkeld tijdens het concert.</li>
                        <li><a href='files/concept-popculture.pdf' target='_blank'>Popculture:</a> Het orkest speelt muziek uit moderne popculture zoals films en games. De campagne is hier ook op gericht.</li>
                        <li><a href='files/concept-community.pdf' target='_blank'>Community:</a> Het concert zelf heeft een lagere drempel omdat mensen makkelijk in en uit kunnen lopen. Ook zijn de zitplekken ingericht voor meer sociaal contact in plaats van het bekijken van het concert.</li>
                        <li><a href='files/concept-spaceconcert.pdf' target='_blank'>Space:</a> De setting van dit concert is de ruimte. De mediacampagne maakt gebruik van de nieuwste technieken zoals VR. Het concert wordt in een planetarium gehouden.</li>
                    </ul>
                    <br>
                    Tijdens de eerste sprintdemo bij Perplex zijn ‘Space’ en ‘Interaction’ gepresenteerd. Na feedback van Perplex is gekozen om ‘Space’ uit te werken tijdens dit semester.<br>
                    <br>
                    <span class='bold'>
                        Communicatiestrategie
                    </span><br>
                    We hebben eerst bieb-onderzoek gedaan waarin wij ontdekten dat het visuele aspect veel beter op het langetermijngeheugen werkt. Vanaf dit punt zijn we gaan kijken welke beelden het beste bij klassieke muziek passen. Hier hebben we interviews voor afgenomen. Uit deze interviews bleek dat “Space” het beste kon worden gebruikt. We hebben de link tussen klassieke muziek en “Space” gelegd aan de hand van sci-fi-films. De doelgroep komt bijna alleen in aanraking met klassieke orkestrale muziek als zij films kijken, en dan zijn het vooral sci-fi- en ruimte-films waarin deze muziek wordt gebruikt. Om deze link te leggen hebben we eerst onderzoek gedaan naar de doelgroep en de interesses van de doelgroep.<br>
                    <br>
                    <span class='bold'>
                        Mediatrategie
                    </span><br>
                    Voor de mediastrategie heb ik onderzoek gedaan naar hoe de website het beste kan worden ingezet. De eerste keer dat de mediastrategie is ingeleverd werd niet genoeg beargumenteerd aan de hand van onderzoek. Na deze feedback ontvangen te hebben is dieper ingegaan op het onderzoek en zijn nieuwe helpvolle tips aan het licht gekomen. Zo kwam naar boven dat voor websites die een sfeer of gevoel over willen brengen een hero-video bovenaan de pagina een positieve invloed heeft op het converteren van bezoekers. Ook is het belangrijk dat het hoofddoel van de website (het kopen van een ticket) boven de ‘fold’ van de hoofdpagina wordt geplaatst.<br>
                    <br>
                    Deze twee bevindingen heb ik vervolgens verwerkt in de website voor de crossmediacampagne. Bovenaan de website is meteen een knop met bijbehorende tekst te vinden die de gebruiker naar de ticketverkoop-sectie leidt. Bovendien speelt op de achtergrond een video met sfeerbeelden zodat meteen een juiste sfeer wordt overgebracht aan de gebruiker.<br>
                    <br>
                    De mediastrategie inclusief communicatiestrategie en communicatiestrategie kan <a href='../files/mediastrategie_latest.pdf' target='_blank'>hier</a> worden gevonden.<br>
                    <br>
                    <span class='bold'>
                        Executie van strategie en concept
                    </span><br>
                    <br>
                    <span class='italic'>
                        hetgeldersorkest.space
                    </span><br>
                    Onderdeel van de campagne is een website waarop tickets kunnen worden gekocht en waar informatie over het concert staat.<br>
                    <a href='http://www.hetgeldersorkest.space' target='_blank'>www.hetgeldersorkest.space</a><br>
                    Ik heb de website ontworpen en samen met Arne Reijntjes gebouwd. De website maakt gebruik van het parallax effect. Dit heb ik gebruikt zodat de grootsheid van het heelal overkomt op de gebruiker van de website. Dit is belangrijk omdat het ook in het concert terug komt.<br>
                    <br>
                    <span class='italic'>
                        Posterserie
                    </span><br>
                    Als zij-opdracht heb ik een serie van 5 posters gemaakt gebaseerd op semiotiek. Voor de posters heb ik “Abel” als font gebruikt. Dit font lijkt veel op het font dat Het Gelders Orkest al gebruikt, maar door de iets rechtere hoeken komt het meer futuristisch over. Het doel van de posters is om bekendheid te wekken bij de doelgroep over Het Gelders Orkest en het aankomende concert. Een ander doel is om mensen naar de website te leiden. Dit wordt gedaan door in te haken op de interesses van de doelgroep. Door de instrumenten uit te beelden in de stijl van Star Wars wordt de link met sci-fi en ruimte gelegd.<br>
                    <br>
                    Er zijn tijdens een SCO-workshop feedbackrondes geweest. Tijdens deze feedbackrondes hebben klasgenoten feedback gegeven op de posterserie. Alle klasgenoten die ik sprak snapten de link tussen de futuristische instrumenten en het concept “Space”. Alleen de short-copy was niet helemaal duidelijk. Het concept spreekt altijd over ruimte, en niet de toekomst. De short-copy was eerst “Neemt je mee naar de toekomst”. Op basis van deze feedback heb ik de short-copy veranderd naar “Neemt je mee naar de ruimte”. Ook stond er bij de eerste feedbackronde nog geen toevoegende informatie bij zoals datum en locatie. Deze zijn ook toegevoegd.<br>
                    <br>
                    <span class='bold'>
                        Tips
                    </span><br>
                    In de eerste versies van veel documenten gebruikten we (bijna) geen bronnen. We namen vooral assumpties mee in onze beslissingen. Hierdoor konden we de keuzes die we hadden gemaakt voor onze producten niet goed onderbouwen. We hebben achteraf nog veel onderzoek gedaan en hebben toen (delen van) producten aangepast. Dit heeft veel dubbel werk opgeleverd.
                </p>",
            "right" => [
                ["image", "../school/s4/sco4.jpg"],
                ["image", "../school/s4/kleuskens_marijn_semiotiek_dirigent2.jpg"],
                ["image", "../school/s4/kleuskens_marijn_semiotiek_viool1-2.jpg"],
                ["image", "../school/s4/kleuskens_marijn_semiotiek_harp2.jpg"],
                ["image", "../school/s4/kleuskens_marijn_semiotiek_drum2.jpg"],
                ["image", "../school/s4/kleuskens_marijn_semiotiek_viool2-2.jpg"]
            ]
        ],
        "externallink" => [
            "url" => "",
            "urltext" => ""
        ]
    ],
    [
        "title" => "User Experience",
        "background" => "../school/s4/ux_intro.jpg",
        "description" => "",
        "content" => [
            "text" =>
                "<p>
                    <span class='bold'>
                        Verplaatsen in doelgroep d.m.v. gebruikersonderzoeken
                    </span><br>
                    Voordat je gaat werken aan een product is het essentieel dat je een doelgroep vaststelt. Des te beter je weet wie de doelgroep is des te beter je het product hier op kunt richten.<br>
                    <br>
                    De doelgroep van deze campagne bestaat uit hoger opgeleide jongeren. Om erachter te komen hoe de campagne het beste kan worden gepresenteerd aan de doelgroep hebben we eerst een onderzoek gedaan naar de interesses van mensen uit de doelgroep. Ook zijn, zeker tijdens het begin van het project, meerdere keren de concepten die waren bedacht getest op de doelgroep.<br>
                    <br>
                    Aan het begin van het project zijn interviews gehouden met mensen uit de doelgroep. Het doel van deze interviews was om erachter te komen wat de mening was over klassieke muziek. Uit deze interviews konden enkele conclusies worden getrokken.<br>
                    <br>
                    Zoals gedacht speelt klassieke muziek geen grote rol in het leven van veel jongeren. Van de 18 geïnterviewden was niemand geïnteresseerd in klassieke muziek. Ook was nog geen geïnterviewde ooit bij een klassiek concert geweest en waren ze niet van plan dit te gaan doen. Als we doorvragen over waarom dit zo is bleek dat veel mensen simpelweg niet erg bekend waren met klassieke muziek. Klassieke muziek speelt geen rol in het leven bij henzelf en hun peers, dus krijgen ze er ook niet snel mee te maken.<br>
                    <br>
                    Als we vroegen wat mensen belangrijk vinden bij concerten worden de sfeer en de mensen waarmee zij gaan als belangrijk beschouwd. Het imago van klassieke concerten is vooral stijf, waarbij de bezoeker op een stoel zit te kijken. Niet erg uitnodigend, vinden de geïnterviewden.<br>
                    <br>
                    Later tijdens het project hebben we interviews gehouden waarbij verschillende concepten werden voorgelegd aan de geïnterviewden. Door dit te doen kon al snel worden gepeild of bepaalde concepten die waren bedacht aan zouden slaan bij de doelgroep. Dit is een makkelijke manier om daarachter te komen zonder dat je tijd investeert in het verder uitwerken van een concept. Er waren 5 concepten uitgewerkt. Deze zijn voorgelegd aan de geïnterviewden. Door uitleg en gebruik van visuals konden goede omschrijvingen van eventuele uitwerkingen worden gegeven zodat de geïnterviewden een goede mening konden geven.<br>
                    <br>
                    Uit deze interviews bleek dat twee concepten er bovenuit staken, namelijk Space en Immersion. Deze twee concepten spraken het meest tot de verbeelding omdat er veel gebruik wordt gemaakt van nieuwe technologie en omdat het concert anders is opgebouwd. Dat het concert niet bestaat uit enkel op een stoel zitten en naar muziek luisteren beviel de geïnterviewden goed.<br>
                    <br>
                    <span class='bold'>
                        Prototypen en usertests
                    </span><br>
                    <br>
                    <span class='italic'>
                        Link tussen space en klassieke muziek
                    </span><br>
                    Er zijn tijdens het project enkele prototypes gemaakt. Om te testen of de link tussen klassieke muziek en ons thema en concept “Space” ook wordt gelegd door de doelgroep heb ik een prototype gemaakt. Het doel van dit prototype is dat de testpersoon verschillende soorten muziek dient te combineren met afbeeldingen. De afbeeldingen geven elk een andere sfeer of thema weer.<br>
                    <br>
                    Alle testpersonen legde de link tussen orkestrale klassieke muziek en de ruimte. Sommige testpersonen twijfelden aan het begin even, maar nadat ze alle muziek hadden gehoord was er geen twijfel meer. Hieruit kon worden geconcludeerd dat we met het concept Space op het juiste pad waren.<br>
                    <br>
                    <span class='italic'>
                        www.hetgeldersorkest.space
                    </span><br>
                    Een onderdeel van de campagne is een website met parallax.<br>
                    <a href='http://www.hetgeldersorkest.space' target='_blank'>www.hetgeldersorkest.space</a><br>
                    Voor deze website heb ik een prototype gemaakt. Het gaat om een technisch werkend prototype, maar is nog niet klaar om gelanceerd te worden omdat de website nog niet responsive is. Ook is de website alleen goed te zien op een resolutie van ongeveer 1300x750 pixels.<br>
                    <br>
                    <span class='italic'>
                        Posters SCO
                    </span><br>
                    Voor een workshop van SCO heb ik een posterserie gemaakt van 5 posters. Het doel van de posters is dat ze het aankomende concert van Het Gelders Orkest beter leren kennen. Een ander doel is om mensen naar de website te leiden.<br>
                    <br>
                    Op de posters heb ik feedback gekregen van klasgenoten. Ze snapten allemaal de link tussen de “StarWars”-instrumenten en het concept. De catchphrase was echter niet duidelijk, omdat ik “Neemt je mee naar de toekomst” had gebruikt, terwijl het concept “Space” nooit over de toekomst praat, maar over de ruimte. Dit en het feit dat er geen verdere informatie in stond als bijvoorbeeld een datum en locatie maakte het niet echt duidelijk waar het over ging. Ik heb van elke poster een tweede versie gemaakt waarin deze fouten zijn rechtgezet.<br>
                    <br>
                    <span class='italic'>
                        Prototype Mens Erger Je Niet
                    </span><br>
                    Tijdens één van de workshops voor UX heb ik een prototype gemaakt voor een vernieuwde Mens Erger Je Niet. De vernieuwde versie is bedoeld voor op een tablet. Als extra uitdaging is het prototype vormgegeven op basis van Google’s Material Design style-guide. De kleuren en vormen die zijn gebruikt komen uit deze style-guide. Dit prototype maakt geen gebruik van een dobbelsteen. In plaats daarvan wordt gebruik gemaakt van ‘rad’ dat rond kan worden gedraaid door er overheen te vegen. Nadat er een getal uitkomt worden de stappen automatisch gezet en draait het veld met daarin het rad naar de speler die vervolgens aan de beurt is.<br>
                    <br>
                    <span class='italic'>
                        Usertest Fontys Portal & Medewerkersplein
                    </span><br>
                    Er is een usertest uitgevoerd om het Fontys portal en medewerkersplein te testen. Om deze test uit te voeren heb ik eerst samen met Marijn Panhuijsen een testplan opgezet waarin vragen zijn opgesteld die vooraf aan de testpersoon worden gesteld. In het testplan zijn ook scenario’s gevormd die aan de testpersoon worden gevraagd om uit te voeren. Door dit te doen komen tekortkomingen van een website makkelijk aan het licht.<br>
                    <br>
                    Voordat de test is uitgevoerd is eerst feedback gevraagd aan de docente. Zij gaf aan dat sommige scenario’s misschien minder goed paste bij het medewerkersplein. Een voorbeeld hiervan is dat enkele scenario’s meer via Canvas worden opgelost dan het medewerkersplein. Toen hebben we onder andere een scenario over het opzoeken van informatie over reiskosten gemaakt. Deze informatie is echt alleen maar te vinden op het medewerkersplein.<br>
                    <br>
                    Persoonlijk heb ik, behalve het maken van het testplan met Marijn Panhuijsen, bij twee tests een uitvoerende rol gehad. Bij één test heb ik genotuleerd en bij een volgende test heb ik zelf de vragen gesteld en de scenario’s voorgelegd.<br>
                    <br>
                    Uit de tests bleek dat de navigatie van het Fontys portal alles behalve efficiënt is. Eén testpersoon gebruikt bijvoorbeeld bookmarks voor zo’n beetje elke pagina zodat hij snel informatie kan vinden zonder door het doolhof te moeten navigeren. Een andere testpersoon gaf aan dat hij bepaalde strings in de url-balk typt en via auto-complete naar een bepaalde pagina gaat, omdat hij anders niet weet hoe hij er moet komen.<br>
                    <br>
                    De resultaten van deze usertest zijn opgenomen in een <a target='_blank' href='files/UsabilityrapportDocentenplein.pdf'>rapport</a> op basis van een template van Usability.gov.<br>
                    <br>
                    <span class='bold'>
                        Tips
                    </span><br>
                    Tijdens het vooronderzoek is door onze groep vooral gebruik gemaakt van interviews om informatie over de doelgroep te vinden. Hiernaast had ook nog gebruik kunnen worden gemaakt van enquêtes. Door alleen interviews te houden is vooral kwalitatieve informatie verstreken, maar weinig kwantitatieve informatie. Er zijn wel Facebook-posts gemaakt met enkele vragen aan de doelgroep erin maar hier is achteraf niet veel nuttige informatie uitgekomen.<br>
                    <br>
                    Samen met Arne Reijntjes heb ik een <a href='files/ABTestwebsite.pdf' target='_blank'>testplan</a> geschreven voor een AB-test van de website. In dit testplan zijn scenario’s geschreven waarmee we verschillende opties op de website willen testen en meten welke betere conversie (het kopen van een ticket) oplevert. Met de scenario’s wilden we het verschil tussen catchphrases, formulieren, hints voor het scrollen en het gebruik van een hero-video meten. Helaas hebben we niet genoeg tijd gehad om de AB-test uit te voeren. Als dit wel was gelukt hadden we de website nog beter af kunnen leveren.<br>
                    <br>
                    <span class='bold'>
                        S5 - Stage
                    </span><br>
                    Tijdens mijn stage bij Third Floor Design heb ik de homepagina van Yogagoods.nl opnieuw ontworpen en heb ik een keuzehelper voor yogamatten toegevoegd aan de site. Daarnaast heb ik ook een FAQ-pagina en andere kleine dingen toegevoegd of aangepast. Voor de nieuwe homepagina en de keuzehelper heb ik veel prototypes gemaakt en usertests gehouden.<br>
                    <br>
                    Voor de keuzehelper zijn paper-prototypes gemaakt. Met deze paper-prototypes kon de manier van interacteren met de keuzehelper worden getest voordat het uiteindelijke ontwerp werd uitgewerkt. Er zijn twee opzetten gemaakt: Vragen stellen en dan een product aanbieden of een veld met filters waarbij telkens producten wegvallen uit een lijst ergens anders op het scherm.<br>
                    Nadat de paper-prototypes waren getest kon worden geconcludeerd dat de opzet met filters niet ideaal is. Dit is omdat als een gebruiker één filter gebruikt er alsnog meerdere producten overblijven die niet de beste keuze zijn. Als eerst alle vragen worden gesteld aan de gebruiker kan worden gegarandeerd dat de beste keuze eruit komt rollen.<br>
                    <br>
                    Toen de keuzehelper op technisch vlak in de prototype-fase belandden is een usertest uitgevoerd. De test is uitgevoerd met de doelgroep bij een yogastudio. Het doel van de test was om erachter te komen of de nieuwe keuzehelper te vinden was en of deze duidelijk was in ontwerp, gebruik en doel.<br>
                    <br>
                    Uit de test bleek dat de navigatie naar de keuzehelper niet goed was, geen enkele testpersoon kon de keuzehelper vinden. Later is op verschillende plekken (waar de testpersonen meteen naar toe bewogen met hun muis) een link naar de keuzehelper toegevoegd. Het doel, de structuur en het ontwerp werd door alle testpersonen als duidelijk ervaren. De testpersonen gaven aan de keuzehelper een waardevolle toevoeging te vinden.
                </p>",
            "right" => [
                ["audio", ["Interview met Connie", "marijn_interview_connie.mp3"]],
                ["audio", ["Interview met Kees", "marijn_interview_kees.mp3"]],
                ["audio", ["Interview met Lukas", "marijn_interview_lukas.mp3"]],
                ["youtube", "<iframe src=\"https://www.youtube.com/embed/dGvwTSbsY1I?rel=0\" frameborder=\"0\" allowfullscreen></iframe>"],
                ["youtube", "<iframe src=\"https://www.youtube.com/embed/videoseries?list=PLvmzcCYIhqVGovSNgCD291x1U593_BS_R\" frameborder=\"0\" allowfullscreen></iframe>"],
                ["image", "../school/s4/mens_erger_je_niet.jpg"],
                ["youtube", "<iframe src=\"https://www.youtube.com/embed/videoseries?list=PLvmzcCYIhqVGvGYpACWYyD9kRMOsA2Gmj\" frameborder=\"0\" allowfullscreen></iframe>"],
                ["image", "../school/s4/ab-test_tickets.jpg"],
                ["image", "../school/s4/stage_paper-prototype.jpg"],
                ["youtube", "<iframe src=\"https://www.youtube.com/embed/videoseries?list=PLvmzcCYIhqVGXD21njbKPVPqB3w9VQ2si\" frameborder=\"0\" allowfullscreen></iframe>"]
            ]
        ],
        "externallink" => [
            "url" => "",
            "urltext" => ""
        ]
    ],
    [
        "title" => "Front end design &amp; development",
        "background" => "../school/s4/development_intro.jpg",
        "description" => "",
        "content" => [
            "text" =>
                "<p>
                    <span class='bold'>
                        Kennis frameworks
                    </span><br>
                    Frameworks of libraries zijn stukken code die je het leven als developer makkelijker maken. In het geval van libraries is dit omdat het stukjes code zijn die en bepaalde functie uitvoeren. Dit hoef je dan zelf niet meer te schrijven. Bij frameworks is het een manier om bijvoorbeeld een website te bouwen. jQuery bijvoorbeeld bestaat uit een hele berg aan JavaScript-functies. Het resultaat is dat jQuery gewoon een makkelijkere manier van JavaScript programmeren is. Voor HTML heb je bijvoorbeeld Bootstrap, een framework om heel makkelijk mobile-friendly websites mee te bouwen. Dit kan met bootstrap doordat er veel voorgekauwde classes inzitten waarmee een website uit kolommen wordt opgebouwd.<br>
                    <br>
                    Frameworks en Libraries worden in de praktijk gebruikt zodat de code gefragmenteerd blijft. Dit houdt in dat de code voor één bepaald onderdeel van een site (bijvoorbeeld een slider) niks te maken heeft met de rest van de site. Hierdoor is het makkelijk om onderdelen los van elkaar bij te werken of te veranderen. Als er iets fout gaat met bijvoorbeeld de slider weet je waar je moet zoeken omdat de code voor de slider niet door de hele codebase ligt verspreid.<br>
                    <br>
                    Nadelen van frameworks zijn dat het voor beperkingen zorgt. De ontwikkelaar van een library of framework kan niet met álle mogelijke gebruiksscenario's rekening houden.<br>
                    <br>
                    <span class='bold'>
                        Keuze library
                    </span><br>
                    Voor dit portfolio is gebruik gemaakt van jQuery. Deze Javascript-library is ingezet voor de animaties van de tekst en de pop-ups bij elke case. jQuery is ingezet omdat het een simpelere en logischere manier van Javascript schrijven is. Ook kan, mocht het nodig zijn, ook standaard Javascript worden gebruikt. Er zijn dus geen grenzen waar je aan vast zit.<br>
                    <br>
                    <span class='bold'>
                        Documentatie – MarijnSlider.js
                    </span><br>
                    De documentatie voor MarijnSlider.js is <a href='http://school.marijnkleuskens.nl/fontys/jaar2/dev42/' target='_blank'>hier</a> te vinden.<br>
                    <br>
                    Ook is de documentatie te vinden in de readme.md in de repository op Gitlab.<br>
                    <br>
                    <span class='bold'>
                        Code – MarijnSlider.js
                    </span><br>
                    MarijnSlider.js is een jQuery plugin. Met deze plugin kan heel makkelijk een slider worden gemaakt. Het enige wat een developer moet doen om MarijnSlider.js te gebruiken is jQuery inladen en een div aanmaken met de class ‘marijn-slider’. In deze div dient nog een div met de class ‘marijn-slides’ geplaatst te worden. Hierin komt een unordered list met elk list-item een slide. In elk list-item kan geplaatst worden wat je wilt.<br>
                    <br>
                    De repository voor MarijnSlider.js is <a href='https://gitlab.com/M4rijn/MarijnSlider/' target='_blank'>hier</a> te vinden.<br>
                    <br>
                    <span class='bold'>
                        Versiebeheer
                    </span><br>
                    Voor dit portfolio en de MarijnSlider-plugin is gebruik gemaakt van GIT. Voor beide zijn repositories aangemaakt op GitLab.com zodat op meerdere systemen aan de code kan worden gewerkt.<br>
                    <ul>
                        <li><a href='https://gitlab.com/M4rijn/portfolio/' target='_blank'>Link naar repository voor het portfolio</a></li>
                        <li><a href='https://gitlab.com/M4rijn/MarijnSlider/' target='_blank'>Link naar repository voor MarijnSlider.js</a></li>
                    </ul>
                    <br>
                    <span class='bold'>
                        Tips
                    </span><br>
                    De library die ik heb gemaakt is niet gebruikt in één van de producten die zijn gemaakt voor de cross-media-campagne. Achteraf is dit natuurlijk jammer omdat het een beetje voelt als onnodig werk.
                </p>",
            "right" => [
                ["marijnslider", "<div class=\"marijn-slider\">
                    <div class=\"marijn-slides\">
                        <ul>
                            <li>
                                <img src='images/school/s4/slide1.jpg' alt='duck' />
                            </li>
                            <li>
                                <img src='images/school/s4/slide2.jpg' alt='road' />
                            </li>
                            <li>
                                <img src='images/school/s4/slide3.jpg' alt='church' />
                            </li>
                        </ul>
                    </div>
                </div>"]
            ]
        ],
        "externallink" => [
            "url" => "",
            "urltext" => ""
        ]
    ],
    [
        "title" => "Search Engine Optimisation ",
        "background" => "../school/s4/seo_intro.jpg",
        "description" => "",
        "content" => [
            "text" =>
                "<p>
                    <span class='bold'>
                        SEO op het portfolio
                    </span><br>
                    Dit portfolio is opgebouwd met behulp van Ajax-calls waarmee de inhoud in de pop-ups wordt ingeladen. Als gevolg hiervan was de aanvankelijke score op SeoSiteCheckup niet zo hoog; namelijk 63.<br>
                    <br>
                    Door enkele tips die SeoSiteCheckup gaf heb ik de score een beetje omhoog weten te halen. Het grootste probleem, de Ajax-calls, zijn in het korte tijdsbestek nog niet opgelost.<br>
                    <br>
                    <span class='bold'>
                        SEO adviesplan
                    </span><br>
                    <span class='italic'>
                        Inleiding
                    </span><br>
                    Normaal crawlt Googlebot door een website om te bepalen wat er op staat en waar de website over gaat. Zo kan Googlebot bepalen hoe hoog de website eindigt op de resultatenpagina bij bepaalde zoekopdrachten. Als er echter gebruik wordt gemaakt van een single-page application (SPA) om een website mee op te bouwen wordt er hoogstwaarschijnlijk gebruik gemaakt van Ajax-calls om content in te laden nadat de pagina voor het eerst is geladen. Het gevolg hiervan is dat Googlebot niet alles kan zien als hij de website indexeert.<br>
                    <br>
                    <span class='italic'>
                        Vroegere oplossing
                    </span><br>
                    Google had een handleiding (Making AJAX applications crawlable, 2015) waarin werd uitgelegd hoe een webdeveloper ervoor kon zorgen dat Googlebot wel toegang had tot content die met Ajax werd aangeleverd. Deze handleiding is sinds 2015 deprecated. Google geeft aan dat content die wordt aangeleverd via Ajax nu automatisch wordt gecrawld. Echter volgens bronnen (Searchengineland, 2015) is dat nog niet altijd het geval.<br>
                    <br>
                    <span class='italic'>
                        Oplossing
                    </span><br>
                    <ul>
                        <li>
                            Serverside Javascript rendering is het renderen van Html met Javascript door de server. Nadat de website is gerendered wordt de HTML naar de gebruiker verstuurd. Het voordeel hiervan is dat bij het eerste keer laden van de pagina meteen alle content aanwezig is. Als elke keer dat een gebruiker nieuwe content opvraagt er een Ajax call moet worden gemaakt duurt het even voordat er iets te zien is. Dit resulteert in een frustrerende en langzame User Experience. Frameworks als React en NodeJS gebruiken serverside Javascript rendering om deze redenen. (Grigoryan, 2017).<br>
                            <br>
                        </li>
                        <li>
                            Progressive enhancement is een manier van opbouwen waarbij eerst het belangrijkste wordt geladen door de browser, namelijk de content en de belangrijkste CSS. De zaken die minder belangrijk of zwaarder zijn worden pas later geladen. Dit zorgt ervoor dat een website snel geladen kan worden en de gebruiker in ieder geval al íets kan doen op de website voordat de hele site is geladen (Wikipedia, z.j.).<br>
                            <br>
                            Hoewel met Javascript alles klikbaar kan worden  gemaakt is het verstandig om hier elementen voor te gebruiken die ervoor bedoeld zijn, zoals anchors en buttons. Als anchors worden gebruikt kan hier ook de link naar de content in worden geplaatst. Deze link kan vervolgens geanimeerd worden. (De Voorhoede, 2016). Samen met bijvoorbeeld de History API (zie bronvermelding) kan ervoor worden gezorgd dat content die wordt ingeladen met Ajax wordt behandeld als een nieuwe pagina door de browser. De gebruiker kan dus de back- en forward-knoppen van de browser blijven gebruiken.<br>
                            <br>
                        </li>
                        <li>
                            Er zijn veel libraries en tools die op een server kunnen worden geïnstalleerd die zogenaamde pre-rendered snapshots maken om het crawlen van websites makkelijker te maken voor bijvoorbeeld Googlebot (Dynomapper, 2017). Door gebruik te maken van deze tools wordt ervoor gezorgd dat alle content correct wordt geïndexeert door zoekmachines.
                        </li>
                    </ul>
                    <br>
                    <span class='italic'>
                        Bronnen
                    </span><br>
                    De Voorhoede. (2016). Progressive Enhancement for JavaScript App Developers. Geraadpleegd van <a target='_blank' href='https://www.voorhoede.nl/en/blog/progressive-enhancement-for-javascript-app-developers/'>https://www.voorhoede.nl/en/blog/progressive-enhancement-for-javascript-app-developers/</a><br>
                    <br>
                    Dynomapper. (2017, 07 mei). How to Make Your JavaScript Website SEO Friendly. Geraadpleegd van <a target='_blank' href='https://dynomapper.com/blog/21-sitemaps-and-seo/245-how-to-make-your-javascript-seo-friendly'>https://dynomapper.com/blog/21-sitemaps-and-seo/245-how-to-make-your-javascript-seo-friendly</a><br>
                    <br>
                    Google. (2015). Making AJAX applications crawlable. Geraadpleegd van <a target='_blank' href='https://developers.google.com/webmasters/ajax-crawling/docs/learn-more'>https://developers.google.com/webmasters/ajax-crawling/docs/learn-more</a><br>
                    <br>
                    Alex Grigoryan (2017, 17 april). The Benefits of Server Side Rendering Over Client Side Rendering. Geraadpleegd van <a target='_blank' href='https://medium.com/walmartlabs/the-benefits-of-server-side-rendering-over-client-side-rendering-5d07ff2cefe8'>https://medium.com/walmartlabs/the-benefits-of-server-side-rendering-over-client-side-rendering-5d07ff2cefe8</a><br>
                    <br>
                    HTML5 Doctor. (2011). Pushing and Popping with the History API. Geraadpleegd van <a target='_blank' href='http://html5doctor.com/history-api/'>http://html5doctor.com/history-api/</a><br>
                    <br>
                    Searchengineland. (2015, 13 november). Can You Now Trust Google To Crawl Ajax Sites? Geraadpleegd van <a target='_blank' href='http://searchengineland.com/can-now-trust-google-crawl-ajax-sites-235267'>http://searchengineland.com/can-now-trust-google-crawl-ajax-sites-235267</a><br>
                    <br>
                    Wikipedia. (z.j.). Progressive enhancement. Geraadpleegd van <a target='_blank' href='https://en.wikipedia.org/wiki/Progressive_enhancement'>https://en.wikipedia.org/wiki/Progressive_enhancement</a>

                </p>",
            "right" => [
                ["image", "../school/s4/seo_eerste_score.png"]
            ]
        ],
        "externallink" => [
            "url" => "",
            "urltext" => ""
        ]
    ],

];

if($_POST){

    $arraySelect = $_POST["arraySelect"];
    $index = $_POST["index"];

    if($arraySelect == "highlightedCases"){
        echo json_encode($highlightedCases[$index]);
    } elseif ($arraySelect == "allCases"){
        echo json_encode($allCases[$index]);
    }

}