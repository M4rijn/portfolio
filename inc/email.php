<?php

if ($_POST) {

    $formInput = $_POST["action"];

    $email_to = "marijn_kleuskens@live.nl";
    $email_subject = "Iemand heeft gereageerd op mijn portfolio";

    // validation expected data exists

    $errors = [];

    if (!isset($formInput["name"]) || !isset($formInput["email"]) || !isset($formInput["message"])) {
        array_push($errors, "Niet alles in het formulier is ingevuld.");
    }

    $name       = $formInput["name"];
    $email_from = $formInput["email"];
    $message    = $formInput["message"];

    // Validate email
    $email_re = '/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD';
    if (!preg_match($email_re, $email_from)) {
        array_push($errors, "Het e-mailadres dat je hebt opgegeven is niet geldig.");
    }

    // Validate name
    $string_re = "/^[A-Za-z .'-]+$/";
    if (!preg_match($string_re, $name)) {
        array_push($errors, "De naam die je hebt opgegeven is niet geldig.");
    }

    if (strlen($message) < 1) {
        array_push($errors, "Het bericht dat je hebt opgegeven is niet geldig.");
    }

    $success = false;

    if (count($errors) > 0) {
        // Errors
        $success = false;
    } else {
        function clean_string($string) {
            $bad = array("content-type", "bcc:", "to:", "cc:", "href");
            return str_replace($bad, "", $string);
        }

        $email_message = "Naam: " . clean_string($name) . "\n";
        $email_message .= "E-mail: " . clean_string($email_from) . "\n";
        $email_message .= "Bericht: " . clean_string($message) . "\n";

        // create email headers
        $headers = "From: marijns@portfolio.nl \r\n" . "Reply-To: " . $email_from . "\r\n" . "X-Mailer: PHP/" . phpversion();

        mail($email_to, $email_subject, $email_message, $headers);

        $success = true;
    }

    echo json_encode(array('success' => $success, $errors));

}