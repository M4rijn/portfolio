<div class="rubrics" id="rubrics">

    <div class="container">

        <h2 class="black">S6 Rubrics</h2>

        <div class="rubrics-table">

            <!-- Table header -->
            <div class="row">
                <div class="col-5th">
                    <h3>Beoordeelingscriteria</h3>
                </div>
                <div class="col-5th">
                    <h3>Allescænner</h3>
                </div>
                <div class="col-5th">
                    <h3>Agnes</h3>
                </div>
                <div class="col-5th">
                    <h3>Amigo</h3>
                </div>
                <div class="col-5th">
                    <h3>Privacy</h3>
                </div>
            </div>

            <!-- Oordeelsvorming -->
            <div class="row">
                <div class="col-5th">
                    <p>Oordeelsvorming</p>
                </div>
                <div class="col-5th">
                    <a class="rubrics-button" href="#pressure cooker: allescænner"></a>
                </div>
                <div class="col-5th">
                    <a class="rubrics-button" href="#pressure cooker: agnes"></a>
                </div>
                <div class="col-5th">
                    <p class="rubrics-table-placeholder">-</p>
                </div>
                <div class="col-5th">
                    <p class="rubrics-table-placeholder">-</p>
                </div>
            </div>

            <!-- Trends -->
            <div class="row">
                <div class="col-5th">
                    <p>Trends</p>
                </div>
                <div class="col-5th">
                    <p class="rubrics-table-placeholder">-</p>
                </div>
                <div class="col-5th">
                    <a class="rubrics-button" href="#pressure cooker: agnes"></a>
                </div>
                <div class="col-5th">
                    <a class="rubrics-button" href="#pressure cooker: amigo"></a>
                </div>
                <div class="col-5th">
                    <a class="rubrics-button" href="#pressure cooker: privacy (deels nog af te ronden)"></a>
                </div>
            </div>

            <!-- Concept -->
            <div class="row">
                <div class="col-5th">
                    <p>Concept</p>
                </div>
                <div class="col-5th">
                    <a class="rubrics-button" href="#pressure cooker: allescænner"></a>
                </div>
                <div class="col-5th">
                    <a class="rubrics-button" href="#pressure cooker: agnes"></a>
                </div>
                <div class="col-5th">
                    <p class="rubrics-table-placeholder">-</p>
                </div>
                <div class="col-5th">
                    <p class="rubrics-table-placeholder">-</p>
                </div>
            </div>

            <!-- Business Model -->
            <div class="row">
                <div class="col-5th">
                    <p>Business Model</p>
                </div>
                <div class="col-5th">
                    <p class="rubrics-table-placeholder">-</p>
                </div>
                <div class="col-5th">
                    <p class="rubrics-table-placeholder">-</p>
                </div>
                <div class="col-5th">
                    <a class="rubrics-button" href="#pressure cooker: amigo"></a>
                </div>
                <div class="col-5th">
                    <p class="rubrics-table-placeholder">-</p>
                </div>
            </div>

            <!-- Marketing -->
            <div class="row">
                <div class="col-5th">
                    <p>Marketing</p>
                </div>
                <div class="col-5th">
                    <p class="rubrics-table-placeholder">-</p>
                </div>
                <div class="col-5th">
                    <a class="rubrics-button" href="#pressure cooker: agnes"></a>
                </div>
                <div class="col-5th">
                    <p class="rubrics-table-placeholder">-</p>
                </div>
                <div class="col-5th">
                    <p class="rubrics-table-placeholder">-</p>
                </div>
            </div>

            <!-- Communicatie -->
            <div class="row">
                <div class="col-5th">
                    <p>Communicatie</p>
                </div>
                <div class="col-5th">
                    <p class="rubrics-table-placeholder">-</p>
                </div>
                <div class="col-5th">
                    <a class="rubrics-button" href="#pressure cooker: agnes"></a>
                </div>
                <div class="col-5th">
                    <p class="rubrics-table-placeholder">-</p>
                </div>
                <div class="col-5th">
                    <a class="rubrics-button in-progress" href="#pressure cooker: privacy (deels nog af te ronden)"></a>
                </div>
            </div>

            <!-- Professionele houding -->
            <div class="row">
                <div class="col-5th">
                    <p>Professionele houding</p>
                </div>
                <div class="col-5th">
                    <p class="rubrics-table-placeholder">-</p>
                </div>
                <div class="col-5th">
                    <a class="rubrics-button" href="#pressure cooker: agnes"></a>
                </div>
                <div class="col-5th">
                    <p class="rubrics-table-placeholder">-</p>
                </div>
                <div class="col-5th">
                    <a class="rubrics-button" href="#pressure cooker: privacy (deels nog af te ronden)"></a>
                </div>
            </div>

            <!-- Design -->
            <div class="row">
                <div class="col-5th">
                    <p>Design</p>
                </div>
                <div class="col-5th">
                    <a class="rubrics-button" href="#pressure cooker: allescænner"></a>
                </div>
                <div class="col-5th">
                    <a class="rubrics-button" href="#pressure cooker: agnes"></a>
                </div>
                <div class="col-5th">
                    <p class="rubrics-table-placeholder">-</p>
                </div>
                <div class="col-5th">
                    <a class="rubrics-button in-progress" href="#pressure cooker: privacy (deels nog af te ronden)"></a>
                </div>
            </div>

            <!-- User Experience -->
            <div class="row">
                <div class="col-5th">
                    <p>User Experience</p>
                </div>
                <div class="col-5th">
                    <a class="rubrics-button" href="#pressure cooker: allescænner"></a>
                </div>
                <div class="col-5th">
                    <p class="rubrics-table-placeholder">-</p>
                </div>
                <div class="col-5th">
                    <p class="rubrics-table-placeholder">-</p>
                </div>
                <div class="col-5th">
                    <p class="rubrics-table-placeholder">-</p>
                </div>
            </div>

            <!-- Gebruikers-onderzoek -->
            <div class="row">
                <div class="col-5th">
                    <p>Gebruikers-onderzoek</p>
                </div>
                <div class="col-5th">
                    <a class="rubrics-button" href="#pressure cooker: allescænner"></a>
                </div>
                <div class="col-5th">
                    <p class="rubrics-table-placeholder">-</p>
                </div>
                <div class="col-5th">
                    <p class="rubrics-table-placeholder">-</p>
                </div>
                <div class="col-5th">
                    <a class="rubrics-button" href="#pressure cooker: privacy (deels nog af te ronden)"></a>
                </div>
            </div>

            <!-- Niveau van product -->
            <div class="row">
                <div class="col-5th">
                    <p>Niveau van product</p>
                </div>
                <div class="col-5th">
                    <a class="rubrics-button" href="#pressure cooker: allescænner"></a>
                </div>
                <div class="col-5th">
                    <p class="rubrics-table-placeholder">-</p>
                </div>
                <div class="col-5th">
                    <p class="rubrics-table-placeholder">-</p>
                </div>
                <div class="col-5th">
                    <a class="rubrics-button in-progress" href="#pressure cooker: privacy (deels nog af te ronden)"></a>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="devider"></div>