<?php
/**
 * Created by PhpStorm.
 * User: Marijn
 * Date: 30/04/2016
 * Time: 18:26
 */

$highlightedCases = [

//    [
//        "title" => "",
//        "background" => "",
//        "description" => "",
//        "content" => [
//            "intro" => "",
//            "chapters" => [
//                "" => [
//                    "text" => [
//                        ""
//                    ],
//                    "right" => [
//                        ["", ""]
//                    ]
//                ],
//            ]
//        ],
//        "externallink" => [
//            "url" => "",
//            "urltext" => ""
//        ]
//    ],

    ////////////////////////////////////////////////////////////// NORMAL CONTENT

    [
        "title" => "Pressure Cooker: Allescænner",
        "background" => "../school/s6/allescaenner/allescaenner.png",
        "description" => "Nieuwe instore marketing",
        "content" => [
            "intro" => "Bij <a href='https://maerschalk.nl/' target='_blank'>Maerschalk</a> heb ik samen met mijn groepsgenoten een opdracht van <a href='http://www.wearenew.nl/' target='_blank'>New Brand Activators</a> uitgewerkt. De opdracht luidde “verzin een innovatieve manier om de instore marketing van de EMTÉ te verbeteren en meetbaar te maken. Denk hierbij ook aan hoe de data wordt weergegeven”. Tijdens het uitwerken van deze opdracht heb ik mij vooral gericht op het onderdeel research, concepting, design, user experience, en de technische uitwerking.",
            "chapters" => [
                "Oplossing" => [
                    "text" => [
                        "Oplossing voor deze probleemstelling is de Allescænner. Dit product is een doorontwikkeling van de zelfscanner die EMTÉ al gebruikt. Er is een beeldscherm ingebouwd in de winkelwagen die in contact staat met de zelfscanner. De winkelwagen kan behalve met het bekende 50 cent-muntje ook met de Fijnproeverspas worden ontgrendeld. Zo weten we wie de winkelkar gebruikt. Op het beeldscherm worden relevante informatie en aanbiedingen getoond voor de betreffende klant, maar ook aan de hand van een gescand product. Dit werkt een op een zelfde manier als veel webshops cross selling inzetten. Omdat je weet welke advertentie aan welke klant wordt getoond weet je of bepaalde aanbiedingen goed werken of niet.",
                        "Buiten dit hoofdonderdeel zijn nog enkele andere features toegevoegd aan Allesænner. Zo wordt het winkellijstje dat je thuis hebt aangemaakt op het beeldscherm getoond en krijg je waarschuwingen als bepaalde stoffen waar je allergisch voor bent in het zojuist gescande product zitten."
                    ],
                    "right" => [
                        ["image", "../school/s6/allescaenner/allescaenner.png"]
                    ]
                ],
                "Oordeelsvorming" => [
                    "text" => [
                        "De probleemstelling voor deze opdracht is best specifiek. De problemen die opgelost dienen te worden zijn vrij concreet beschreven. Dit betekend dat er vanaf het begin een gericht onderzoek kan worden gedaan.",
                        "De hoofdvraag die beantwoord dient te worden is “Welke innovatieve technologieën kunnen worden gebruikt om de instore marketing in de EMTÉ te verbeteren en meetbaar te maken?”",
                        "Om een beeld te krijgen bij wat de EMTÉ precies is en wat de identiteit van het bedrijf is ben ik op bezoek gegaan bij twee filialen om een veldonderzoek te doen. Eén filiaal ingericht volgens de EMTÉ 2.0 formule, en één ingericht volgens de EMTÉ 3.0 formule.",
                        "Voordat er een begin werd gemaakt met het uitvoeren van het concept zijn interviews gehouden met klanten in de EMTÉ. Ik heb 5 mensen geïnterviewd over hun winkelervaring binnen de EMTÉ. Met deze inzichten kan ik het concept wat al deels rond was rond deze fase aanscherpen. Bijvoorbeeld door suggesties van de geïnterviewden te integreren binnen het concept. Een concreet voorbeeld hiervan is een melding over een allergie die je hebt als je een product scant die een stof bevat waarvoor je allergisch voor bent."
                    ],
                    "right" => [
                        ["audio", ["", "projects/school/s6/allescaenner/marijn_richard_man_30.mp3"]],
                        ["audio", ["", "projects/school/s6/allescaenner/marijn_richard_man_40.mp3"]],
                        ["audio", ["", "projects/school/s6/allescaenner/marijn_richard_mevrouw_30.mp3"]],
                        ["audio", ["", "projects/school/s6/allescaenner/marijn_richard_mevrouw_50-60.mp3"]],
                        ["audio", ["", "projects/school/s6/allescaenner/marijn_richard_mevrouw_70.mp3"]],
                    ]
                ],
                "Concept" => [
                    "text" => [
                        "De vraag die werd gesteld door <a href='http://www.wearenew.nl/' target='_blank'>New Brand Activators</a> was om offline media in de winkel meetbaar te maken. We hebben als groep gekozen om niet de huidige media (posters, vloerstickers, etc.) trackbaar te maken, maar om een nieuwe manier van instore marketing te ontwikkelen. Door gebruik te maken van een scherm in de winkelkar die is gekoppeld  aan de zelfscanner van EMTÉ kunnen relevante aanbiedingen worden getoond op het scherm. Bovendien kan de klant de winkelkar ontgrendelen met zijn of haar Fijnproeverspas. Dit houdt in dat ook persoonlijke aanbiedingen kunnen worden getoond.",
                        "Er kan makkelijk worden bijgehouden welke klant bepaalde aanbiedingen te zien krijgt. Daarnaast kun je ook meten of vervolgens het aangeboden product later wordt gescand. Je kunt dus bekijken welke aanbiedingen bij welke producten het beste werken. Met deze informatie kun je itereren om zo tot beter resultaten te komen.",
                        "Om op dit concept te komen hebben we eerst individueel een concept bedacht en uitgewerkt. Ik heb me bezig gehouden met het uitwerken van het concept “Smartcart”. Dit concept geeft binnen de winkel gerichte advertenties aan bezoekers. Het winkelkarretje of -mandje is voorzien van beacons die communiceren met zenders binnen de winkel. Zo kan worden bijgehouden waar een bezoeker zich bevindt. Hierdoor kan worden bijgehouden of een klant ergens lang staat te twijfelen bij een product. Door middel van de scanner kan worden bepaald of er iets in het winkelwagentje wordt gestopt. Zo niet dan kan er een relevante persoonlijke aanbieding worden getoond op een van de schermen binnen de winkel.",
                        "Uiteindelijk is een deel van mijn concept gebruikt in het uiteindelijke concept dat als groep is uitgewerkt."
                    ],
                    "right" => [
                        ["image", "../school/s6/allescaenner/visualisatie_beacons.jpg"]
                    ]
                ],
                "Design" => [
                    "text" => [
                        "Het design heb ik bijna volledig op me genomen. De interface van het scherm heb ik vormgegeven volgens de designguidelines van EMTÉ. De winkelketen is vrij recent overgestapt naar een volledig nieuwe huisstijl. Hier heb ik dankbaar gebruik van gemaakt, omdat de oude huisstijl niet van deze tijd is.",
                        "Persoonlijk vind ik het uiteindelijke ontwerp goed gelukt omdat het duidelijk opgezet is. De verschillende functies worden met grote knoppen weergegeven. Hier heb ik expres op gelet omdat de doelgroep erg breed is, en dus misschien minder technisch onderlegd. Als het op stijl aankomt vind ik dat ik me goed aan het <a href='files/projects/school/s6/allescaenner/Huisstijldocument.pdf' target='_blank'>stijlboard</a> - dat Branco heeft gemaakt - heb gehouden. Ik heb elk onderdeel een eigen vak gegeven dat altijd hetzelfde blijft, waardoor het makkelijk te bedienen en te volgen blijft."
                    ],
                    "right" => [
                        ["image", "../school/s6/allescaenner/laatste_gescande_product.png"]
                    ]
                ],
                "User Experience" => [
                    "text" => [
                        "Hassenzahl’s be-goals zijn een bepaald soort doelen die je stelt aan een interactief product. Deze be-goals slaan op hoe jij wilt dat een gebruiker zich voelt, of wat een gebruiker “is” tijdens het gebruik van jouw product.",
                        "Vaak wordt wel rekening gehouden met zogenaamde do-goals, zoals “Ik moet ervoor zorgen dat de gebruiker op deze knop klikt zodat hij naar de productpagina komt” of “Minstens 10% van de bezoekers van mijn website moeten verder komen dan de landingpagina”.",
                        "Be-goals hebben een andere insteek. Bij be-goals wordt rekening gehouden met doelen zoals “Ik wil dat de gebruiker zich gewaardeerd voelt” of “Ik wil dat de gebruiker gestimuleerd wordt”. Als aan de beoogde be-goals kan worden voldaan is de kans zeer groot dat de gebruiker een positieve ervaring met het product zal hebben, omdat er een fijn gevoel achter blijft.",
                        "De be-goals van Hassenzahl zijn <span class ='strong'>Stimulation</span>: Je bent gestimuleerd om het product te gebruiken en voelt je hier prettig bij, <span class ='strong'>Relatedness</span>: Je voelt vaak intiem contact met mensen die om je geven, <span class ='strong'>Competence</span>: Je voelt je competent en effectief met de acties die je uitvoert en <span class ='strong'>Popularity</span>: Je voelt je bevriend en gerespecteerd.",
                        "Tijdens het ontwerpen van de Allescænner heb ik rekening gehouden met de be-goals van Hassenzahl. Door deze regels in mijn achterhoofd te houden zorg ik ervoor dat er een positief gevoel achterblijft bij de gebruiker terwijl en nadat het product gebruikt is. De gebruiker voelt zich prettig bij het gebruik van de Allescænner  omdat er gerelateerde en persoonlijke suggesties worden gegeven (<span class ='strong'>Stimulation</span>). Er wordt bijgehouden welke producten je vaker koopt en op basis hiervan krijg je suggesties voor nieuwe producten waar je zelf misschien niet aan had gedacht (<span class ='strong'>Relatedness</span>). De Allescænner geeft je de meest effectieve route door de winkel heen om je boodschappenlijstje af te werken (<span class ='strong'>Competence</span>). Nadat je klaar bent met het boodschappen doen kun je stemmen op welk product je volgende week in de aanbieding wilt, zo heb je zelf invloed op waar je korting op krijgt (<span class ='strong'>Popularity</span>)."
                    ],
                    "right" => [
                        ["image", "../school/s6/allescaenner/hassenzahl.jpg"]
                    ]
                ],
                "Gebruikers-onderzoek" => [
                    "text" => [
                        "Voordat er een begin werd gemaakt met het uitvoeren van het concept zijn interviews gehouden met klanten in de EMTÉ. Ik heb 5 mensen geïnterviewd over hun winkelervaring binnen de EMTÉ. Daarnaast heb ik vragen gesteld over ons concept, een scherm in een winkelkar. Dit heb ik gedaan om alvast een peiling te krijgen van wat mensen van dit idee vinden. Uit deze interviews bleek dat een groot deel van de klanten de ideeën die wij als groep hadden wel zagen zitten. Mede vanwege deze reden hebben we met zelfvertrouwen kunnen kiezen voor dit concept.",
                        "Nadat een eerste prototype is gemaakt zijn we met de hele groep gaan testen in de EMTÉ. We hebben willekeurige klanten het prototype uit laten proberen en gevraagd wat zij ervan vonden. Wat voor ons belangrijk was, was of de ondervraagde klanten meerwaarde zagen in het concept. Vanaf het begin vonden we het belangrijk dat de getoonde aanbiedingen niet te opdringerig moesten worden. Daarom heb ik hier ook specifiek naar gevraagd tijdens de interviews. Uit de usertests bleek dat veel klanten de relevante aanbiedingen juist handig vonden, en niet opdringerig zoals door ons gevreesd.",
                    ],
                    "right" => [
                        ["youtube", "<iframe src=\"https://www.youtube.com/embed/5W1VENe3pZk?rel=0\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>"],
                        ["audio", ["", "projects/school/s6/allescaenner/marijn_richard_man_30.mp3"]],
                        ["audio", ["", "projects/school/s6/allescaenner/marijn_richard_man_40.mp3"]],
                        ["audio", ["", "projects/school/s6/allescaenner/marijn_richard_mevrouw_30.mp3"]],
                        ["audio", ["", "projects/school/s6/allescaenner/marijn_richard_mevrouw_50-60.mp3"]],
                        ["audio", ["", "projects/school/s6/allescaenner/marijn_richard_mevrouw_70.mp3"]],
                    ]
                ],
                "Niveau product" => [
                    "text" => [
                        "Het prototype dat gebouwd is voor dit project is vrij ver uitgewerkt. Met een telefoon kan een barcode worden gescand (voor dit prototype ter vervanging van de zelfscanner van EMTÉ). De gegevens van de barcode worden via websockets naar de hoofdwebsite gestuurd. Zo kan zonder opnieuw pagina’s laden telkens een nieuw product worden gescand. Met Javascript wordt gezorgd dat het juiste product weergegeven op het scherm in de winkelkar als een product met de telefoon wordt gescand. Daarnaast zijn de andere features zoals een interactieve boodschappenlijst en zoekfunctie ook werkend gemaakt.",
                        "De code voor dit project is te vinden in de <a href='https://github.com/M4rijn/pressurecooker_emte' target='_blank'>Github repository</a>."
                    ],
                    "right" => [
                        ["image", "../school/s6/allescaenner/js_emte.png"]
                    ]
                ]
            ],
        ],
        "externallink" => [
            "url" => "",
            "urltext" => ""
        ]
    ],

    [
        "title" => "Pressure Cooker: Agnes",
        "background" => "../school/s6/agnes/volkswagen.png",
        "description" => "Langdurige marketingcampagne",
        "content" => [
            "intro" => "Bij <a href='http://www.wearenew.nl/' target='_blank'>New Brand Activators</a> hebben wij de opdracht gekregen om een campagne van 4 jaar te ontwikkelen. De opdracht kwam via <a href='https://www.greenhousegroup.nl/' target='_blank'>Greenhouse Group</a> van Volkswagen (fictief) en het doel van de campagne is om meer mensen in zelfrijdende Volkswagens te krijgen die over 4 jaar op de markt komen.",
            "chapters" => [
                "Oplossing" => [
                    "text" => [
                        "De campagne die wij op hebben gezet begint met een hoax. Veel autofabrikanten maken al gebruik van apps om de status van jouw auto naar je telefoon te sturen. Wij vragen bekende vloggers uit de technologie- en autobranche om te vertellen dat zij steeds meer creepy berichten van hun huidige auto’s ontvangen via deze apps. De berichten worden in een anderhalf jaar opgebouwd van “de benzine is bijna leeg” naar “Ik wil nu een rondje gaan rijden!”. Hierdoor wordt aan de doelgroep het idee van een AI met een bewustzijn geloofwaardig."
                    ],
                    "right" => [
                        ["youtube", "<iframe src=\"https://www.youtube.com/embed/YoDxC6Gw9sI?rel=0\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>"]
                    ]
                ],
                "Oordeelsvorming" => [
                    "text" => [
                        "In tegenstelling tot het eerste project van dit semester (Allescænner), is de briefing voor dit project erg breed. Om te oriënteren op het onderwerp ben ik begonnen met een <a href='files/projects/school/s6/agnes/onderzoek_carsharing.pdf' target='_blank'>kort biebonderzoek naar car sharing</a>, omdat dit een groot onderdeel van het product is dat gemarket dient te worden.",
                        "De hoofdvraag die beantwoord dient te worden is “Hoe zorg je ervoor dat de aandacht voor jouw campagne voor 4 jaar vast wordt gehouden onder de doelgroep?”",
                        "Naast het korte biebonderzoek heb ik ook onderzoek gedaan naar succesvolle langdurige campagnes. Ik heb de campagnes van <a href='https://vimeo.com/30280801' target='_blank'>True Blood</a> en <a href='http://www.42entertainment.com/work/whysoserious' target='_blank'>The Dark Knight</a> geanalyseerd. Uit deze analyses heb ik de onderdelen proberen te halen die de campagnes zo succesvol hebben gemaakt. Uit deze analyses blijkt dat het belangrijk is om niet meteen alles vrij te geven. Door mensen steeds kleine beetjes informatie te geven houdt je ze lang geprikkeld. Een voorbeeld hiervan is dat de campagne van True Blood eerst cryptische brieven naar bloggers (over horror) stuurt. Deze influencers hebben een groot bereik binnen de doelgroep om de campagne te kickstarten. Vanuit dit punt kun je de campagne langzaam over een langere periode opbouwen."
                    ],
                    "right" => [
                        ["image", "../school/s6/agnes/True-Blood-Logo.jpg"]
                    ]
                ],
                "Trends" => [
                    "text" => [
                        "Deze ronde heb ik het trendonderzoek op mij genomen. Door een trendonderzoek te doen leg je op een overzichtelijke manier de relevante trends vast waardoor je weet waar je rekening mee moet houden waar je op in kunt spelen.",
                        "Deze opdracht is gericht op het delen van een zelfrijdende auto die over vier jaar uit komt. De campagne moet echter nu al starten. Er moet dus gekeken worden naar huidige trends en trends in de toekomst. Als naar de doelgroep wordt gekeken valt op dat deze steeds minder naar traditionele media zoals TV en Radio kijkt en luistert. Zij bevinden zich bijna alleen maar op social media. Als naar technieken wordt gekeken valt op dat zelfrijdende auto’s en elektrische auto’s de laatste tijd flink in opkomst zijn. Daarnaast valt op dat kunstmatige intelligentie steeds complexer wordt. Met deze bevindingen kan vervolgens naar een concept toe worden gewerkt.",
                        "Ik heb voor dit trendonderzoek gebruik gemaakt van een consumer trend canvas. Hierdoor wordt duidelijk wat de behoeften van de doelgroep zijn, waar de trend vanaf komt en waar het heen gaat."
                    ],
                    "right" => [
                        ["image", "../school/s6/agnes/consumer-trend-canvas.jpg"]
                    ]
                ],
                "Concept" => [
                    "text" => [
                        "De conceptfase voor dit project duurde lang, omdat de opdracht erg breed was en er geen concrete vraagstelling vanuit de briefing naar voren kwam. Ik ben samen met mijn groep gaan brainstormen via verschillende methodes. Zo zijn bijvoorbeeld de <a href='images/school/s6/agnes/agnes_concept_pro_cons.png' target='_blank'>voor- en nadelen van car sharing, elektrische auto’s en zelfrijdende auto’s</a> op een rij gezet om te kijken waarop de focus kan worden gelegd in de campagne. Hier is uiteindelijk geen tof concept gekomen. Vervolgens is samen met Jurgen en Lisanne van <a href='http://www.wearenew.nl/' target='_blank'>New Brand Activators</a> een sparsessie gehouden om ideeën op elkaar af te schieten. Uit deze sessie ben ik op het idee gekomen om een hoax op te bouwen: “Wat als een zelfrijdende auto zo slim is dat hij een eigen persoonlijkheid krijgt en echt gaat leven?”"
                    ],
                    "right" => [
                        ["image", "../school/s6/agnes/agnes_concept_pro_cons.png"]
                    ]
                ],
                "Marketing" => [
                    "text" => [
                        "De briefing voor dit project was om een marketingcampagne te ontwikkelen die vier jaar duurt. De doelgroep die werd gegeven was erg breed: 10 tot 99-jarigen. Gelukkig werd hierin een focus op young professionals gelegd. Het doel van de opdracht is dus om de doelgroep klaar te stomen voor car sharing met een zelfrijdende Volkswagen die over 4 jaar op de markt komt.",
                        "Ik heb eerst onderzoek gedaan naar deze doelgroep zodat hier efficiënter op gericht kan worden. Young professionals zijn mensen die van 20 tot 35 jaar oud en hebben een kantoorbaan. Uit dit doelgroeponderzoek blijkt dat een groot deel van deze groep naar het werk of school gaat met het OV. Daarom is ervoor gekozen om een deel van de campagne op het OV te richten.",
                        "Uit mijn doelgroeponderzoek blijkt ook dat de doelgroep zich ook vooral bevindt op social media en minder op traditionele media. Om deze reden is bepaald om de kick-off van de campagne te doen met hulp van bekende vloggers uit de auto- en techbranche. Deze personen (influencers) hebben veel impact op onze doelgroep.",
                        "Uit mijn onderzoek naar succesvolle langdurige campagnes (zie “Oordeelsvorming”) blijkt dat het belangrijk is om de campagne in fases op te bouwen (in dit geval: acceptatie, introductie, integratie en implementatie). Hierdoor blijft de doelgroep langer geïnteresseerd, omdat er een gevoel van progressie in het verhaal zit. Er wordt in de campagne een hoax opgebouwd die zich steeds verder uitzet en begint te escaleren. Op het hoogtepunt van de hoax wordt Agnes geïntroduceerd, VW’s eigen kunstmatige intelligentie.",
                        "Het doel van de campagne wordt bereikt door Agnes te introduceren, Volkswagen’s eigen kunstmatige intelligentie die wordt gebruikt in VW’s huidige modellengamma. Klanten van Volkswagen raken zo al gewend aan het idee van een AI in hun auto die hen assisteert. Agnes assisteert in alles wat je maar kunt bedenken, waaronder deels zelfrijdend. Hierdoor positioneert VW zich als vooruitstrevend merk dat naar de toekomst kijkt, maar ook naar veiligheid omdat het ‘probleem’ dat in de hoax is ontstaan wordt opgelost. Als er straks een zelfrijdende Volkswagen uitkomt is het logisch omdat VW al langer bezig was met zelfrijdende functies in hun auto’s."
                    ],
                    "right" => [
                        ["image", "../school/s6/agnes/vw-marketing.jpg"]
                    ]
                ],
                "Communicatie" => [
                    "text" => [
                        "De pitch voor dit project heb ik samen met Selime gepresenteerd. Ik heb ook de opbouw en vormgeving van de slides op mij genomen. We hebben elk onze eigen tekst geschreven en daarna elkaar feedback gegeven daarover. Zo zijn we op teksten gekomen die het concept goed uitleggen. Nadat de teksten waren bepaald hebben we de laatste twee dagen meerdere keren per dag geoefend zodat we de teksten uit ons hoofd zouden kennen met de eindpresentatie. Daarnaast hebben we de presentatie ook twee keer bij <a href='http://www.wearenew.nl/' target='_blank'>New Brand Activators</a> gedaan voor verschillende collega’s zodat ook zij nog feedback konden geven.",
                        "Uiteindelijk met de eindpresentatie is alles goed gegaan. Ik kwam een paar keer niet goed uit mijn woorden, maar verder ben ik wel tevreden."
                    ],
                    "right" => [
                        ["youtube", "<iframe src=\"https://www.youtube.com/embed/HY-w6v4jctY\" frameborder=\"0\" allowfullscreen></iframe>"]
                    ]
                ],
                "Professionele houding" => [
                    "text" => [
                        "Nadat de eerste twee projecten zijn voltooid hebben we als groep elkaar een peerreview gegeven. <a href='files/projects/school/s6/agnes/Peerreview-Marijn1.pdf' target='_blank'>Ik ben beoordeeld op verschillende punten door mijn groepsgenoten</a>.",
                        "Ik ben door mijn groepsgenoten op drie punten onvoldoende beoordeeld, namelijk initiatief nemen, voor kritiek open staan en na gaan wat reëel is voor kritiek. Persoonlijk kan ik me wel vinden in deze meningen, vooral op initiatief nemen. In de volgende twee projecten van de Pressure Cooker ga ik focus leggen op deze onderdelen van mijn persoonlijke ontwikkeling om deze te verbeteren."
                    ],
                    "right" => [
                        ["image", "../school/s6/agnes/peerreview1.png"]
                    ]
                ],
                "Design" => [
                    "text" => [
                        "De presentatie heb ik zelf vormgegeven. Het doel dat ik wilde bereiken met de vormgeving voor de slides is een futuristische sfeer creëren die het concept goed zou ondersteunen. Hierbij heb ik ook gebruik gemaakt van het <a href='files/projects/school/s6/agnes/Huisstijldocument.pdf' target='_blank'>huisstijldocument</a> dat Selime heeft gemaakt.",
                        "Jurgen van <a href='http://www.wearenew.nl/' target='_blank'>New Brand Activators</a> zei tijdens de oefenpitches dat hij de vormgeving van de slides goed bij het concept vond passen en dat de opbouw duidelijk was."
                    ],
                    "right" => [
                        ["image", "../school/s6/agnes/ontwerp_pitch_agnes.png"]
                    ]
                ]
            ]
        ],
        "externallink" => [
            "url" => "",
            "urltext" => ""
        ]
    ],

    [
        "title" => "Pressure Cooker: Amigo",
        "background" => "../school/s6/amigo/nao.png",
        "description" => "Nao Robot",
        "content" => [
            "intro" => "Bij <a href='https://www.handpickedagencies.com/' target='_blank'>Handpicked Agencies</a> heb ik samen met mijn groep medestudenten gewerkt aan een oplossing om licht verstandelijk beperkten (LVB’ers) meer regie te geven over hun eigen leven. Dit is gedaan met hulp van de Nao robot.",
            "chapters" => [
                "Onze oplossing" => [
                    "text" => [
                        "Tijdens het onderzoek zijn we erachter gekomen dat bijna elke LVB’er tegen hetzelfde probleem aanloopt: planning. Ze hebben moeite met een planning maken, onderhouden en volgen. Dit heeft verstrekkende gevolgen voor de LVB’ers zelf en hun begeleiders. Omdat LVB’ers zelf geen planning kunnen maken en onderhouden moet de taak worden overgenomen door hun begeleiders. Dit neemt veel tijd in die beter kan worden besteed. Daarnaast, als de planning anders loopt dan gedacht werd door een LVB’er raakt deze persoon gestrest.",
                        "De oplossing die hiervoor gemaakt is, is een robot die in de gedeelde woonkamer van een zorgtehuis rondloopt en alle inwoners te woord kan staan. Als een begeleider een gesprek heeft met een cliënt en er wordt over de planning gepraat kan de robot de planning automatisch aanpassen. Uiteraard vraagt de robot om toestemming voordat hij iets in de planning aanpast.",
                        "LVB’ers hebben moeite met articulatie. Dit maakt een spraakinterface met een robot onmogelijk te gebruiken door deze mensen. Om dit op te lossen is gebruik gemaakt van een scherm dat is ingebouwd in de robot. Via dit scherm kunnen de cliënten klikken op symbolen die op agenda-items slaan. Hierbij kun je denken aan symbolen voor de kapper, eten, tuinieren, etc. De robot kan met behulp van gezichtsherkenning zien wie er voor hem staat, dus als een cliënt op een symbool tikt kan de robot tegen deze persoon vertellen wanneer het betreffende agenda-item op de planning staat.",
                        "Door gebruik te maken van deze robot zijn begeleiders minder tijd kwijt aan het maken en onderhouden van een planning voor hun cliënten. LVB’ers weten altijd waar ze aan toe zijn."
                    ],
                    "right" => [
                        ["image", "../school/s6/amigo/logo.png"]
                    ]
                ],
                "Trends" => [
                    "text" => [
                        "Tijdens dit project heb ik me bezig gehouden met een trendonderzoek. Hiervoor heb ik een <a href='files/projects/school/s6/amigo/Kleuskens_Marijn_Robots_mogelijkheden.pdf' target='_blank'>onderzoek gedaan naar robots die particulier te koop zijn</a> en hoe deze te gebruiken zijn binnen ons concept. Ik heb naar 5 individuele robots onderzoek gedaan, namelijk Nao (die we tot onze beschikking hadden), Pepper, Romeo, Aibo en Kirobo. Voor elke robot heb ik vervolgens de voor- en nadelen op een rijtje gezet.",
                        "Met deze informatie heb ik een goed advies aan mijn projectgroep kunnen geven. Op basis van de kennis die ik met dit onderzoek heb opgedaan is met inspraak van de groep een weloverwogen keuze gemaakt om het concept te baseren op Pepper. Er is gekozen voor Pepper omdat LVB’ers vaak moeite hebben met uitspraak en daarom een scherm een betere interface is. Pepper heeft een ingebouwd scherm dus dat komt perfect van pas.",
                    ],
                    "right" => [
                        ["image", "../school/s6/amigo/kirobo.jpg"]
                    ]
                ],
                "Business-model" =>[
                    "text" => [
                        "Tijdens het ontwikkelen van Amigo heb ik de taak om een business-model te maken op me genomen. Door gebruik te maken van het Business Canvas Model worden de kosten en opbrengsten van een product of dienst in beeld gebracht.",
                        "Nadat het onderzoek is afgerond en er een concept is uitgewerkt moeten alle <a href='files/projects/school/s6/amigo/Kleuskens_marijn_Kosten_amigo.pdf' target='_blank'>kosten op een rijtje worden geplaatst</a>. Naast het business-model heb ik ook hiervoor gezorgd. Op basis van mijn trendonderzoek, kostenplaatje en onderzoek naar welke benodigdheden er zijn voor het concept heb ik een <a href='files/projects/school/s6/amigo/Amigo_business-model-canvas_verantwoording.pdf' target='_blank'>business-model opgezet</a>.",
                        "<span class='strong'>Feedback Erdinç Saçan</span><br>Nadat ik het business-model gemaakt had, heb ik het opgestuurd naar Erdinç Saçan met de vraag of hij feedback zou kunnen geven.",
                        "Het Value Propositions veld is niet sterk genoeg omdat hieruit niet duidelijk wordt waarom bedrijven, instellingen of klanten het product moeten kopen. Ik zou hier meer concrete cijfers in kunnen zetten, zodat duidelijk wordt welke voordelen en opbrengsten de robot met zich meebrengt.",
                        "Bij Customer Relationships moet staat hoe een klant ons kan bereiken. Hier zou dus moeten worden beschreven dat wij Amarant als klant hebben.",
                        "Onder het kopje Revenu Streams is niet concreet en duidelijk aangegeven wat nu de opbrengsten zijn. De begeleiders hebben meer tijd, maar wat levert dit op. Hier zou verder onderzoek naar moeten worden gedaan.",
                        "Het kopje Cost Structure is ook niet duidelijk genoeg. Wie maakt de software, hoeveel mensen en uren zijn hiervoor nodig? Uit het kostenplaatje kan die makkelijk worden opgemaakt, dus het had wel in het business-model gekund."
                    ],
                    "right" => [
                        ["image", "../school/s6/amigo/Amigo_business-model-canvas.png"]
                    ]
                ]
            ],
        ],
        "externallink" => [
            "url" => "",
            "urltext" => ""
        ]
    ],

    [
        "title" => "Pressure Cooker: Privacy (deels nog af te ronden)",
        "background" => "../school/s6/privacy/privacy_background.png",
        "description" => "B2B Service",
        "content" => [
            "intro" => "Bij <a href='https://www.greenhousegroup.nl/' target='_blank'>Greenhouse Group</a> heb ik samen met mijn groep een opdracht van <a href='https://maerschalk.nl/' target='_blank'>Maerschalk</a> uitgewerkt. De opdracht was om een digitale oplossing te ontwikkelen omtrent privacy voor het Nederlandse volk.",
            "chapters" => [
                "Onze oplossing" => [
                    "text" => [
                        "Om dit project te starten hebben we eerst als groep een groot onderzoek gehouden. We hebben interviews gehouden en een enquête opgezet. Er zijn 15 mensen geïnterviewd en de enquête is 116 keer ingevuld. Daarnaast is een biebonderzoek gedaan naar de GDPR/AVG.",
                        "Uit het onderzoek is gebleken dat een grote meerderheid van de ondervraagden geen probleem hebben met privacy. De problemen die rondom privacy bestaan lost de nieuwe wetgeving veelal op. Om deze redenen zijn we gaan kijken naar de mogelijkheden die het delen van data te bieden heeft.",
                        "Online krijg je veel gepersonaliseerde advertenties, maar offline gebeurt dit nooit. Door jouw online profiel te gebruiken in een fysieke winkel kan de shopervaring flink worden verbeterd. Om dit concept uit te werken wordt een hologram bij de ingang van een winkel geplaatst die jou persoonlijk begroet en je op de hoogte brengt van aanbiedingen die voor jou van toepassing zijn. Dit heeft voor de winkelbezoeker voordelen, omdat bijvoorbeeld een man van 25 jaar oud minder advertenties voor vrouwenshampoo te zien krijgt en een winkeleigenaar is minder geld kwijt aan reclame-uitingen die op een deel van de bezoekers verloren gaat."
                    ],
                    "right" => [
                        ["", ""]
                    ]
                ],
                "Trends" => [
                    "text" => [
                        "Het concept dat is ontwikkeld voor deze opdracht vraagt naar een manier van interactie met een klant in een winkel. Om uit te zoeken wat hier een goede optie voor is heb ik een <a href='files/projects/school/s6/privacy/Kleuskens_Marijn_Coolhunt_interaction.pdf' target='_blank'>Coolhunt Verslag</a> gemaakt waarin ik trends binnen Human Computer Interface heb onderzocht. Met een Coolhunt Verslag maak je een duidelijk overzicht van macro-, maxi- en microtrends binnen een bepaald onderwerp. Door hier een duidelijk overzicht van te creëren kan goed in worden gespeeld op lopende en opkomende trends, zodat een groter publiek kan worden bereikt.",
                        "Opkomende trends voor interactie met computers zijn VR/AR en hologrammen. Deze twee trends heb ik geanalyseerd op macro-, maxi- of microniveau om er zo achter te komen hoe de trends binnen het concept zouden kunnen passen.",
                        "Ons concept heeft een manier van interactie met de klant binnen de winkel nodig. Omdat een medewerker niet altijd tijd heeft hebben we aan de hand van mijn trendonderzoek gekozen om hologrammen te gebruiken zodat er toch een vorm van menselijk contact is."
                    ],
                    "right" => [
                        ["image", "../school/s6/privacy/coolhunt_hologram_tupac.jpg"]
                    ]
                ],
                "Communicatie (nog af te ronden)" => [
                    "text" => [
                        "Aan het einde van deze ronde van de Pressure Cooker ga ik de pitch doen samen met Selime. De laaste keer dat ik de pitch deed (Agnes) kwam ik een paar keer niet uit mijn woorden. Ik hoop dat als ik nog meer mijn teksten oefen, het niet meer gebeurd."
                    ]
                ],
                "Professionele houding" => [
                    "text" => [
                        "Net voordat het laatste project is afgerond hebben we als groep elkaar weer <a href='files/projects/school/s6/privacy/Peerreview-Marijn2.pdf' target='_blank'>feedback gegeven over onze professionele houding</a>. We hebben hetzelfde peerreview-formulier erbij gepakt en hebben elkaar beoordeeld.",
                        "Mijn persoonlijke doel was om te richten op initiatief nemen, open staan voor kritiek en nagaan wat reëel is in de ontvangen kritiek. Op deze 3 punten had ik namelijk een onvoldoende gescoord tijdens de vorige keer dat we elkaar beoordeeld hadden (zie Pressure Cooker: Agnes).",
                        "Ik was blij te zien dat mijn groepsgenoten me dit keer op alle drie deze onderdelen wel een voldoende of zelfs goed gaven. Dit heb ik bereikt omdat ik me hier bewust op heb gefocust."
                    ],
                    "right"=> [
                        ["image", "../school/s6/privacy/peerreview2.png"]
                    ]
                ],
                "Design (nog af te ronden)" => [
                    "text" => [
                        "De slides voor de pitch dienen nog gemaakt te worden. Ik ga de slides dezelfde stijl geven als ik het prototype geef, zodat een samenhangend geheel ontstaat."
                    ]
                ],
                "Gebruikers-onderzoek" => [
                    "text" => [
                        "Om een beter beeld te krijgen over hoe de Nederlandse bevolking tegenover privacy staat heb ik enkele interviews afgenomen. Er is met de hele groep een vragenlijst opgesteld en vervolgens zijn we gesplit om zo meer mensen te kunnen ondervragen.",
                        "Het doel van dit interview is om erachter te komen hoe mensen tegen privacy aankijken en vooral waarom zij hun meningen hebben. Met een interview kan meer informatie uit een persoon worden gehaald dan met bijvoorbeeld een enquête, ook al stel je dezelfde vragen. Omdat je de mogelijkheid hebt om door te vragen als een antwoord je opvalt kun je dieper op de materie ingaan.",
                        "Uit de interviews bleek dat veel mensen niet veel problemen of zorgen om privacy hebben. Met deze informatie is vervolgens ook een enquête opgesteld met meer gerichte vragen. De interviews hebben dus erg veel nut gehad om een eerste richting te kiezen voor het uiteindelijke concept."
                    ],
                    "right" => [
                        ["youtube", "<iframe src=\"https://www.youtube.com/embed/76l_0SyHnp0\" frameborder=\"0\" allowfullscreen></iframe>"]
                    ]
                ],
                "Niveau van product (nog af te ronden)" => [
                    "text" => [
                        "Samen met Selime ben ik bezig om het visuele onderdeel van het hologram uit te werken. We doen dit door met een green screen te filmen en vervolgens in de nabewerking enkele effecten toe te voegen. Omdat het hologram op verschillende vragen antwoord moet geven is het van belang dat het begin en einde van elke actie die wordt gefilmd bijna gelijk is, zodat het altijd vloeiend in elkaar overloopt, ongeacht de volgorde."
                    ]
                ]
            ]
        ],
        "externallink" => [
            "url" => "",
            "urltext" => ""
        ]
    ],

    [
        "title" => "Het Gelders Orkest",
        "background" => "../school/s4/hetgeldersorkest-showcase.png",
        "description" => "Crossmedia marketingcampagne",
        "content" => [
            "intro" => "Via <a href='https://www.perplex.nl/' target='_blank'>Perplex</a> heb ik de opdracht gekregen om samen met een groep van 4 medestudenten een campagne voor Het Gelders Orkest te maken. Het doel van de campagne is om meer jongeren naar hun optredens te trekken.",
            "chapters" => [
                "Strategie &amp; concept" => [
                    "text" => [
                        "Aan het begin van het semester zijn verschillende concepten bedacht. Om op deze concepten te komen is gebruik gemaakt van verschillende soorten technieken om mee te brainstormen.",
                        "We zijn begonnen met het bedenken van 200 ideeën. Het maakte niet uit hoe haalbaar of onhaalbaar deze ideeën waren. Doordat er zoveel willekeurige dingen worden opgeschreven kom je uiteindelijk op ideeën waar je eerst nooit aan had gedacht.",
                        "Het resultaat van 3 weken brainstormen was 5 concepten. Deze concepten zijn uitgewerkt met de “Slapend, ontwakend, levend”-methode. Door gebruik te maken van deze methode wordt meteen duidelijk wat het waarom en bestaansrecht van een concept zijn (slapend), wat de pay-off en/of proposities zijn (ontwakend) en hoe het concept wordt omgezet in producten of diensten (levend). Bij Perplex zijn 2 concepten gepitched, waarna één concept werd gekozen om uit te werken.",
                        "Het uiteindelijke concept is geworden: \"Space\". Uit onderzoek blijkt dat jongeren bijna alleen nog in contact komen met klassieke muziek bij het kijken en spelen van sci-fi films en -games. Ons uitgangspunt voor het concept was dus: Een concert in de ruimte.",
                        "Voor de mediastrategie heb ik onderzoek gedaan naar hoe de website het beste kan worden ingezet. De eerste keer dat de mediastrategie is ingeleverd werd niet genoeg beargumenteerd aan de hand van onderzoek. Na deze feedback ontvangen te hebben is dieper ingegaan op het onderzoek en zijn nieuwe helpvolle tips aan het licht gekomen. Zo kwam naar boven dat voor websites die een sfeer of gevoel over willen brengen een hero-video bovenaan de pagina een positieve invloed heeft op het converteren van bezoekers. Ook is het belangrijk dat het hoofddoel van de website (het kopen van een ticket) boven de ‘fold’ van de hoofdpagina wordt geplaatst.",
                        "Deze twee bevindingen heb ik vervolgens verwerkt in de website voor de crossmediacampagne. Bovenaan de website is meteen een knop met bijbehorende tekst te vinden die de gebruiker naar de ticketverkoop-sectie leidt. Bovendien speelt op de achtergrond een video met sfeerbeelden zodat meteen een juiste sfeer wordt overgebracht aan de gebruiker.",
                        "De mediastrategie inclusief communicatiestrategie kan <a href='files/mediastrategie_latest.pdf' target='_blank'>hier (1,3 MB)</a> worden gevonden."
                    ],
                    "right" => [
                        ["image", "../school/s4/sco4.jpg"],
                        ["audio", ["Interview met Connie", "marijn_interview_connie.mp3"]],
                        ["audio", ["Interview met Kees", "marijn_interview_kees.mp3"]],
                        ["audio", ["Interview met Lukas", "marijn_interview_lukas.mp3"]],
                    ]
                ],
                "User Experience" => [
                    "text" => [
                        "De doelgroep van deze campagne bestaat uit hoger opgeleide jongeren. Om erachter te komen hoe de campagne het beste kan worden gepresenteerd aan de doelgroep heb ik eerst een onderzoek gedaan naar de interesses van mensen uit de doelgroep. Ook zijn, zeker tijdens het begin van het project, meerdere keren de concepten die waren bedacht getest op de doelgroep.",
                        "Zoals gedacht speelt klassieke muziek geen grote rol in het leven van veel jongeren. Van de 18 geïnterviewden was niemand geïnteresseerd in klassieke muziek. Ook was nog geen geïnterviewde ooit bij een klassiek concert geweest en waren ze niet van plan dit te gaan doen. Als we doorvragen over waarom dit zo is bleek dat veel mensen simpelweg niet erg bekend waren met klassieke muziek. Klassieke muziek speelt geen rol in het leven bij henzelf en hun peers, dus krijgen ze er ook niet snel mee te maken.",
                        "Als we vroegen wat mensen belangrijk vinden bij concerten worden de sfeer en de mensen waarmee zij gaan als belangrijk beschouwd. Het imago van klassieke concerten is vooral stijf, waarbij de bezoeker op een stoel zit te kijken. Niet erg uitnodigend, vinden de geïnterviewden.",
                        "Er zijn tijdens het project enkele prototypes gemaakt. Om te testen of de link tussen klassieke muziek en ons thema en concept “Space” ook wordt gelegd door de doelgroep heb ik een prototype gemaakt. Het doel van dit prototype is dat de testpersoon verschillende soorten muziek dient te combineren met afbeeldingen. De afbeeldingen geven elk een andere sfeer of thema weer."
                    ],
                    "right" => [
                        ["youtube", "<iframe src=\"https://www.youtube.com/embed/dGvwTSbsY1I?rel=0\" frameborder=\"0\" allowfullscreen></iframe>"],
                        ["youtube", "<iframe src=\"https://www.youtube.com/embed/videoseries?list=PLvmzcCYIhqVGovSNgCD291x1U593_BS_R\" frameborder=\"0\" allowfullscreen></iframe>"]
                    ]
                ]
            ]
        ],
        "externallink" => [
            "url" => "",
            "urltext" => ""
        ]
    ],
    [
        "title" => "Stage Third Floor Design: Yogagoods.nl",
        "background" => "yogagoods/yogagoods-showcase.png",
        "description" => "Website",
        "content" => [
            "intro" => "Yogagoods.nl is een webshop dat zich richt op yogaproducten. Tijdens mijn stage bij Third Floor Design heb ik de homepagina aangepast, een keuzehelper gemaakt en een FAQ-sectie toegevoegd.",
            "chapters" => [
                "Homepagina" => [
                    "text" => [
                        "De oude homepagina was erg inefficiënt opgebouwd. Er was bijvoorbeeld een knop waar veel bezoekers op klikten die nergens naar toe ging. Onder andere deze fout heb ik recht getrokken in een nieuw ontwerp dat nu live staat.",
                    ],
                    "right" => [
                        ["image", "yogagoods/design_yogagoods.png"]
                    ]
                ],
                "Keuzehelper" => [
                    "text" => [
                        "Aan Yogagoods.nl is ook een keuzehelper toegevoegd. De bedoeling van de keuzehelper is het makkelijker maken voor yogi’s om erachter te komen welke yogamat de beste keuze is voor hun gebruiksscenario. Aan de hand van enkele vragen wordt een ideale yogamat aangeraden."
                    ],
                    "right" => [
                        ["image", "yogagoods/keuzehelper.jpg"]
                    ]
                ],
                "FAQ-sectie" => [
                    "text" => [
                        "Als laatste heb ik ook een FAQ voor Yogagoods.nl gemaakt. De FAQ had twee doelen: Het aantal mensen dat dagelijks belt voor relatief simpele vragen verminderen en proberen nieuwe bezoekers te trekken via <a href='https://support.google.com/webmasters/answer/6229325?hl=en' target='_blank'>Google Featured Snippets</a>. De FAQ is dan ook opgebouwd volgens de richtlijnen van Google om in een Featured Snippet te komen."
                    ],
                    "right" => [
                        ["image", "yogagoods/ontwerp_faq.png"]
                    ]
                ]
            ]
        ],
        "externallink" => [
            "url" => "files/stageverslag_kleuskens_marijn.pdf",
            "urltext" => "Bekijk stageverslag (6,5 MB)"
        ]
    ],
    [
        "title" => "Twee Werelden",
        "background" => "tweewerelden/tweewerelden-showcase.png",
        "description" => "Web app",
        "content" => [
            "intro" => "Twee Werelden is een web app gericht op basisschoolleerlingen van groep 7 en 8.",
            "chapters" => [
                "Briefing" => [
                    "text" => [
                        "De opdrachtgever voor dit project is <a href='http://www.digitaslbi.com/nl/' target='_blank'>DigitasLBI</a>, een marketingbureau uit Amsterdam. Zij hebben ons gebriefd over dit project. De opdracht die zij ons gaven was: Geef vorm aan een passage uit het dagboek van Anne Frank dat jullie het meeste aanspreekt en probeer deze in de dagelijkse leefwereld van een kind te plaatsen."
                    ],
                    "right" => [
                        ["image", "tweewerelden/logo_digitaslbi.jpg"]
                    ]
                ],
                "Wat is Twee Werelden?" => [
                    "text" => [
                        "Ik heb Twee Werelden met 5 medestudenten gemaakt: <br> <ul><li>Youri Gruiters</a></li><li>Kevin Karsoem</a></li><li>Hicham Tijani</a></li><li>Connie Smits</li><li>Iris van Stiphout</li></ul>",
                        "Met Twee Werelden wordt het verhaal van Anne Frank verteld door links te leggen met de huidige vluchtelingensituatie.",
                        "De web app wordt gebruikt in een klaslokaal als een interactief spel. Twee groepen leerlingen van elk 3-5 personen spelen met elkaar, elke groep heeft een eigen tablet of computer. De twee groepen kunnen enkel via de app met elkaar communiceren, en niet fysiek. De twee groepen bevinden zich elk in een andere kamer in het spel. Eén groep speelt in de kamer van Anne Frank, de andere in een kamer in een asielzoekerscentrum (AZC).",
                        "De opdracht die aan de leerlingen wordt geven is het beantwoorden van verschillende vragen. De antwoorden op deze vragen liggen in de andere kamer. De leerlingen gaan op zoek naar manieren om met de andere groep te communiceren. Het communiceren gaat via tijdsbruggen, zoals bijvoorbeeld een foto in een envelop van Anne Frank die als snapchat aankomt in de kamer van het AZC. Naast deze tijdsbrug zijn er nog meerdere manieren waarop gecommuniceerd kan worden.",
                        "Door het ontdekken en gebruiken van deze tijdsbruggen komen de leerlingen erachter dat de leefsituatie van Anne Frank en de vluchtelingen van de huidige tijd niet ver uit elkaar ligt."
                    ],
                    "right" => [
                        ["image", "tweewerelden/tweewerelden-kamers.png"]
                    ]
                ]
            ]
        ],
        "externallink" => [
            "url" => "",
            "urltext" => ""
        ]
    ],

];

$allCases = [

    [
        "title" => "MedQit",
        "background" => "medqit/medqit-showcase.png",
        "description" => "Android app",
        "content" => [
            "intro" => "MedQit is een Android app met als doel het medisch dossier te vervangen. Zowel medische experts als patiënten hebben toegang tot data van de patiënt.",
            "chapters" => [
                "Achtergrond" => [
                    "text" => [
                        "De opdracht is gebriefd door Drs. Herm Martens, een dermatoloog aan het Catharinaziekenhuis Eindhoven en Maastricht. Het doel van de opdracht is om een manier te vinden waar efficiënter om wordt gegaan met data van een patiënt. Een voorbeeld uit de praktijk is dat mensen die rond hun 20ste bepaalde problemen hebben met hun gebit later in hun leven vaak last krijgen van huidkanker. Tandartsen weten dit niet, lossen simpelweg het probleem op en klaar is kees."
                    ],
                    "right" => [
                        ["", ""]
                    ]
                ],
                "Onze oplossing: MedQit" => [
                    "text" => [
                        "Ik heb MedQit met 4 medestudenten gemaakt:<br><ul><li><a href='http://marcolemmens.com/' target='_blank'>Marco Lemmens</a></li><li><a href='http://joeygeraeds.nl/' target='_blank'>Joey Geraeds</a></li><li>Wouter Noij</li><li>Hein Dauven</li></ul>",
                        "Onze oplossing op het probleem is een platform dat onder andere het patiëntendossier vervangt. De patiënt bepaald met dit platform zelf welke gegevens hij of zij deelt met verschillende medische experts. Als een patiënt inlogt krijgt hij/zij op het hoofdscherm een overzicht te zien van alle ‘lopende’ klachten. Er kan dan per klacht worden bekeken wat de status is aan de hand van een tijdlijn. Per klacht kan ook het dossier worden geopend. Het dossier bestaat uit verschillende componenten, die elk op een onderdeel van de behandeling slaan. Voorbeelden hiervan zijn medicijnen die gebruikt worden, oefeningen die worden gedaan en statistieken van het lichaam.",
                        "Als een patiënt naar een afspraak met een expert gaat kan de data worden getoond op een groot scherm met behulp van Android TV. Met dit grote scherm kan makkelijk de voortgang en de planning van het hersteltraject worden besproken."
                    ],
                    "right" => [
                        ["image", "medqit/medqit-showcase.png"]
                    ]
                ]
            ]
        ],
        "externallink" => [
            "url" => "",
            "urltext" => ""
        ]
    ],
    [
        "title" => "P'APP",
        "first-image" => "papp/papp-square.jpg",
        "description" => "iOS App",
        "content" => [
            "text" =>
                "<p>
                    <span class='italic'>
                        P'app is een iOS app waarmee je fysiek papieren vliegtuigjes met berichten daarop de wereld in kunt gooien. Deze vliegtuigjes kunnen door anderen worden opgepakt en verder gegooid.
                    </span><br>
                    <br>
                    <span class='bold'>
                        Achtergrond
                    </span><br>
                    Een half jaar lang hadden wij Fontys-studenten een project lopen bij <a href='https://www.greenhousegroup.nl/' target='_blank'>Greenhouse Group</a>. De briefing was om een app te maken die de data-overflow waar veel mensen tegenwoordig aan lijden te verminderen.<br>
                    <br>
                    Ons concept leunt op het idee dat er op social media erg veel onzin bij de nuttige informatie wordt geplaatst. Hierdoor krijgen mensen veel dingen te zien waar ze niet op zitten te wachten.<br>
                    P'app zorgt ervoor dat mensen alleen berichten te zien krijgen van hoge kwaliteit.<br>
                    <br>
                    <span class='bold'>
                        Wat is P'app
                    </span><br>
                    Ik heb P'app met 5 medestudenten gemaakt:
                </p>
                <ul>
                    <li>- <a href='http://marcolemmens.com/' target='_blank'>Marco Lemmens</a></li>
                    <li>- <a href='http://joeygeraeds.nl/' target='_blank'>Joey Geraeds</a></li>
                    <li>- <a href='http://nard.nl/' target='_blank'>Nard Broekstra</a></li>
                    <li>- Wouter Noij</li>
                    <li>- Mick Wonnink</li>
                </ul>
                <p>
                    <br>
                    In P'app kun je een bericht maken en dit op een papieren vliegtuigje plaatsen. Dit vliegtuigje kun je vervolgens weggooien. Nadat je het vliegtuigje weggooit komt het ergens 'fysiek' in de wereld te liggen. Als andere gebruikers van de app in de buurt komen van een geland vliegtuigje kunnen zij deze oppakken, lezen en verder gooien.<br>
                    <br>
                    Als de lezer een bericht goed vind, kan hij of zij deze beoordelen. De originele gooier van het vliegtuigje krijgt hier 'experience points' voor. Hoe meer xp-punten je hebt, hoe verder en vaker je een vliegtuigje de wereld in kunt gooien. Hiermee zorgen wij ervoor dat er geen of weinig onzinberichten de wereld ingegooid worden.<br>
                    <br>
                    <span class='bold'>
                        ICTalent Awards
                    </span><br>
                    Wij zijn met deze app tot de finale van de ICTalent Awards gekomen. Helaas is er na de halve finale niet veel tijd geweest om de app uit haar prototype-fase te halen.<br>
                    <br>
                    <span class='bold'>
                        Wat ik heb gedaan
                    </span><br>
                    Ik ben vooral voorantwoordelijk geweest voor het UI- en UX-ontwerp van de app. Daarnaast heb ik talloze kleine dingetjes gedaan van campagneposters tot documentatie van het project.
                </p>",
            "right" => [
                ["image", "papp/papp-1.png"],
                ["image", "papp/papp-2.png"]
            ]
        ],
        "externallink" => [
            "url" => "",
            "urltext" => ""
        ]
    ],
    [
        "title" => "We Are Data",
        "first-image" => "wearedata/wearedata-square.jpg",
        "description" => "Interactieve installatie",
        "content" => [
            "text" =>
                "<p>
                    <span class='italic'>
                        We Are Data is een vereniging die de bewustwording van big data in Nederland wil verbeteren.
                    </span><br>
                    <br>
                    <span class='bold'>
                        Achtergrond
                    </span><br>
                    We Are Data is een half jaar lang project opgezet door de stichting <a href='http://wearedata.nl/' target='_blank'>We Are Data</a>. We Are Data heeft een container gemaakt waarin een tal van sensoren zijn geplaatst; van vingerafdrukscanners in de deurknop en weegschalen in de vloer tot eye-trackers en Xbox Kinect. Deze container wordt op verschillende openbare plekken en festivals in Nederland geplaatst om zo zoveel mogelijk mensen te trekken.<br>
                    <br>
                    <span class='bold'>
                        Project
                    </span><br>
                    Ik heb dit project met 4 medestudenten gemaakt, namelijk:
                </p>
                <ul>
                    <li>- Wouter Noij</li>
                    <li>- <a href='http://conniesmits.nl/' target='_blank'>Connie Smits</a></li>
                    <li>- Youri Gruiters</li>
                    <li>- Hicham Tijani</li>
                </ul>
                <p>
                    <br>
                    Aan ons was de taak om een programma te verzinnen dat zich binnen de container afspeelt, dus wat er met de sensoren wordt gedaan. Na een aantal weken brainstormen zijn we tot een concept gekomen waar we met zijn allen tevreden over waren.<br>
                    <br>
                     <span class='bold'>
                        Concept
                    </span><br>
                    Ons concept maakt vooral gebruik van vooroordelen. Het werkt als volgt: je komt bij de container, die ergens in Nederland staat, en vult een kort formuliertje in om je aan te melden. Vervolgens stap je de container in en krijg je verschillende scenario's te zien gefilmd vanuit een eerste-persoons-perspectief. De scenario's die worden geschetst zijn dingen als in een café biertjes drinken, wakker worden en uit je bed stappen, door een donker steegje lopen, etc.<br>
                    <br>
                    Na elk filmpje krijgt de bezoeker een vraag te horen en drie keuzes te zien; drie willekeurige bezoekers die vóór hem of haar ook in de container zijn geweest. Dus bijvoorbeeld bij het scenario waar je opstaat krijgt de gebruiker de vraag \"naast wie zou je het liefst wakker willen worden?\" en kan dan kiezen. De gebruiker kan nergens op klikken, maar met de eye-tracker kunnen wij zien wie de gebruiker kiest, zonder dat hij of zij het zelf door heeft.<br>
                    <br>
                    Nadat je alle scenario's hebt gezien en uit de container loopt krijg je een visitekaartje met daarop een code waarmee je kunt inloggen op de website van We Are Data. Immers nu jij in de container bent geweest kom jij ook tussen de keuzemogelijkheden van bezoekers die na jou komen en kun je zien hoe anderen jou beoordelen op basis van alleen een foto.
                </p>",
            "right" => [
                ["image", "wearedata/wearedata-1.jpg"],
                ["image", "wearedata/wearedata-2.png"]
            ]
        ],
        "externallink" => [
            "url" => "http://school.marijnkleuskens.nl/fontys/jaar2/ptm31/usertest/",
            "urltext" => "Bekijk usertest"
        ]
    ],
    [
        "title" => "Youseum",
        "first-image" => "youseum/youseum-square.jpg",
        "description" => "iOS App",
        "content" => [
            "text" =>
                "<p>
                    <span class='italic'>
                        Youseum is een iOS-app gemaakt voor museumrondleiding met behulp van iBeacons.
                    </span><br>
                    <br>
                    Youseum is een iOS app die ik samen met Wouter Noij heb gemaakt. Met deze app worden audiotours op goedkope plastic mp3-spelertjes en halfwerkende koptelefoons vervangen.<br>
                    <br> 
                    Doormiddel van iBeacons wordt de locatie van de bezoeker binnen het museum bijgehouden. Als een bezoeker dicht bij een bepaald kunstwerk in de buurt komt wordt het bijbehorende audiobestand afgespeeld. Er wordt ook rekening gehouden met de algemene audiotours die door het hele museum lopen.
                </p>",
            "right" => [
                ["image", "youseum/youseum-1.jpg"],
                ["image", "youseum/youseum-2.jpg"]
            ]
        ],
        "externallink" => [
            "url" => "",
            "urltext" => ""
        ]
    ],
    [
        "title" => "Digital Magazine",
        "first-image" => "digital_magazine/digital_magazine-square.jpg",
        "description" => "Schoolopdracht",
        "content" => [
            "text" =>
                "<p>
                    <span class='italic'>
                        Voor een schoolopdracht heb ik een digitaal magazine ontworpen. Het bestaat uit fotografie, een 'geremixt' artiekel en een graphic novel.
                    </span><br>
                    <br>
                    <span class='bold'>
                        Achtergrond
                    </span><br>
                    Dit project is uitgesmeerd over het hele tweede semester van het eerste jaar van mijn ICT & Mediadesign opleiding. Ik heb eerst enkele weken les gehad in fotografie, grafisch ontwerp en illustreren. Voor elk onderdeel heb ik een tussenproject opgeleverd en uiteindelijk zijn deze projecten samengevoegd in één digitaal magazine geplaatst.
                </p>",
            "right" => [
                ["image", "digital_magazine/digital_magazine-1.jpg"]
            ]
        ],
        "externallink" => [
            "url" => "",
            "urltext" => ""
        ]
    ],
    [
        "title" => "Studio Swiss",
        "first-image" => "studio_swiss/studio_swiss-square.jpg",
        "description" => "Website",
        "content" => [
            "text" =>
                "<p>
                    <span class='italic'>
                        Studio Swis is een fictief designbureau opgezet door 4 studenten ICT & Mediadesign.
                    </span><br>
                    <br>
                    <span class='bold'>
                        Achtergrond
                    </span><br>
                    Studio Swis is een proftaak die ik heb uitgevoerd in het laatste semester van leerjaar 1 van de opleiding ICT & Mediadesign. Ik heb samen met drie klasgenoten een fictief mediadesignbureau opgezet en gebrand. Mijn collega's in dit project waren:<br>
                    <br>
                </p>
                <ul>
                    <li>- Stan Zeetsen</li>
                    <li>- Kevin Karsoem</li>
                    <li>- Tom Dijkstra</li>
                </ul>
                <p>
                    <br>
                    Studio Swis 'is' een jong en fris mediadesignbureau dat zich richt op kleine bedrijven. De diensten die 'wij' aanbieden zijn: webontwerp, webontwikkeling, branding en concultancy.<br>
                    <br>
                    <span class='bold'>
                        Wat heb ik gedaan
                    </span><br>
                </p>
                <ul>
                    <li>- Concept bedenken (samen met de anderen)</li>
                    <li>- Logo ontwerp (Uiteindelijke logo is een samensmelting van twee voorstellen)</li>
                    <li>- Alle pagina's grafisch vormgegeven</li>
                    <li>- Grafisch ontwerp (Ook dit is een samensmelting van twee voorstellen)</li>
                    <li>- De website bouwen</li>
                </ul>
                <p>
                    <br>
                    <span class='bold'>
                        Gebruikte technieken
                    </span><br>
                </p>
                <ul>
                    <li>- HTML / CSS</li>
                    <li>- jQuery</li>
                </ul>",
            "right" => [

            ]
        ],
        "externallink" => [
            "url" => "http://school.marijnkleuskens.nl/fontys/jaar1/22/studioswis/",
            "urltext" => "Bekijk website"
        ]
    ],
    [
        "title" => "Lakesilence",
        "first-image" => "lakesilence/lakesilence-square.jpg",
        "description" => "Website",
        "content" => [
            "text" =>
                "<p>
                    <span class='italic'>
                        Lakesilence is een band van de Rockacadamy. De band vroeg studenten van de opleiding ICT & Mediadesign of zij een website voor de band wilden ontwikkelen.
                    </span><br>
                    <br>
                    <span class='bold'>
                        Achtergrond
                    </span><br>
                    Lakesilence is een band van de Rockacadamy. Als deel van hun afstudeerproject moeten zij een album uitbrengen. Dit houdt ook alle heisa eromheen, dus ook een website.<br>
                    <br>
                    De band heeft contact opgenomen met Fontys om te vragen of het een leuke opdracht was voor de studenten van de opleiding ICT & Media design.<br>
                    <br>
                    De website is gemaakt door een groepje van vier studenten die elk een aparte rol hadden in dit project.<br>
                    <br>
                    Voor deze website heb ik zelf een klein CMS gemaakt met PHP. Dit heeft het grootste deel van de tijd op zich genomen, maar is aan de voorkant helaas niet te zien.<br>
                    <br>
                    <span class='bold'>
                        Wat heb ik gedaan
                    </span><br>
                </p>
                <ul>
                    <li>- Stijl bepaald</li>
                    <li>- Logo ontworpen</li>
                    <li>- Alle pagina's grafisch vormgegeven</li>
                    <li>- Website ontwikkeld</li>
                    <li>- Een CMS voor de website ontwikkeld</li>
                </ul>
                <p>
                    <span class='bold'>
                        Gebruikte technieken
                    </span><br>
                </p>
                <ul>
                    <li>- HTML / CSS</li>
                    <li>- PHP</li>
                </ul>",
            "right" => [

            ]
        ],
        "externallink" => [
            "url" => "http://school.marijnkleuskens.nl/fontys/jaar1/21/lakesilence/",
            "urltext" => "Bekijk website"
        ]
    ],
    [
        "title" => "Starteenwebshop",
        "first-image" => "starteenwebshop/starteenwebshop-square.jpg",
        "description" => "Website",
        "content" => [
            "text" =>
                "<p>
                    <span class='italic'>
                        Starteenwebshop is een website gemaakt voor Mindworkz. Via deze site kun je gemakkelijk een eigen webshop opstarten met hulp van de professionals van Mindworkz.
                    </span><br>
                    <br>
                    <span class='bold'>
                        Achtergrond
                    </span><br>
                    Als meesterproef heb ik voor Mindworkz de website Starteenwebshop ontworpen en gebouwd. De site is bedoeld om mensen die een simpele webshop willen beginnen een oplossing te bieden.<br>
                    <br>
                    De website is eerst opgebouwd met HTML/CSS en later in een CMS gezet. Als CMS heb ik gebruik gemaakt van Concrete5. Dit CMS wordt veel gebruikt binnen Mindworkz, en er is dus veel kennis over dit CMS aan boord.<br>
                    <br>
                    <span class='bold'>
                        Wat heb ik gedaan
                    </span><br>
                </p>
                <ul>
                    <li>- Stijl bepaald</li>
                    <li>- Alle pagina's grafisch uitgewerkt</li>
                    <li>- Website gebouwd</li>
                </ul>
                <p>
                    <span class='bold'>
                        Gebruikte technieken
                    </span><br>
                </p>
                <ul>
                    <li>- HTML / CSS</li>
                    <li>- jQuery</li>
                    <li>- PHP</li>
                    <li>- Concrete5</li>
                </ul>",
            "right" => [
                ["image", "starteenwebshop/starteenwebshop-1.png"]
            ]
        ],
        "externallink" => [
            "url" => "",
            "urltext" => ""
        ]
    ],
    [
        "title" => "Tomiko",
        "first-image" => "tomiko/tomiko-square.jpg",
        "description" => "Interactieve menukaart",
        "content" => [
            "text" =>
                "<p>
                    <span class='italic'>
                        Een interactieve menukaart voor sushirestaurant Tomiko dat wordt weergegeven in beeldschermen die in de tafel zijn ingebouwd.
                    </span><br>
                    <br>
                    <span class='bold'>
                        Achtergrond
                    </span><br>
                    Dit project heb ik gemaakt als examenopdracht voor mijn opleiding Interactieve Vormgeving aan het Sint Lucas, Eindhoven. De bedoeling was dat er een interactieve menukaart moest worden ontworpen voor een fictief Japans restaurant. De menukaart zou worden worden weergegeven op beeldschermen die in de tafels zijn ingebouwd.<br>
                    <br>
                    Gasten zitten aan een grote tafel, met ongeveer 20 personen, en de kok in het midden. De gasten kunnen via de menukaart hun gerechten bestellen, die de kok dan kan klaarmaken.<br>
                    <br>
                    De kok heeft ook een eigen scherm, waar hij kan zien wat de huidige bestellijst is. De kok kan hier ook zien wat het snelst klaar is of wat het eerst is besteld.
                </p>",
            "right" => [
                ["image", "tomiko/tomiko-1.jpg"],
                ["image", "tomiko/tomiko-2.jpg"],
                ["image", "tomiko/tomiko-3.jpg"],
                ["image", "tomiko/tomiko-4.jpg"],
                ["image", "tomiko/tomiko-5.jpg"],
                ["image", "tomiko/tomiko-6.jpg"],
                ["image", "tomiko/tomiko-7.jpg"]
            ]
        ],
        "externallink" => [
            "url" => "http://school.marijnkleuskens.nl/sintlucas/tomiko/",
            "urltext" => "Bekijk prototype"
        ]
    ]
];

if($_POST){

    $arraySelect = $_POST["arraySelect"];
    $index = $_POST["index"];

    if($arraySelect == "highlightedCases"){
        echo json_encode($highlightedCases[$index]);
    } elseif ($arraySelect == "allCases"){
        echo json_encode($allCases[$index]);
    }

}