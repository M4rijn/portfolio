<!DOCTYPE HTML>

<html>

<head>

    <?php

//        define("HOST", "htttp://www.marijnkleuskens.nl");

//        $subdomain = explode(".", $_SERVER['HTTP_HOST'])[0];
//        $fileDir = "";
//        if($subdomain === "school"){
//            $fileDir = "../";
//        }
    ?>

    <title>Portfolio Marijn Kleuskens</title>
    <meta name="description" content="Persoonlijk portfolio van Marijn Kleuskens, webdesigner en webdeveloper.">
    <meta name="keywords" content="Marijn Kleuskens Portfolio Webdesign Web Design Developer Eindhoven">
    <meta name="author" content="Marijn Kleuskens">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <link rel="canonical" href="marijnkleuskens.nl" />

    <!-- Open Sans -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,600italic,700,800,800italic,700italic,300italic' rel='stylesheet' type='text/css'>
    <!-- Monsterrat -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

    <!-- CSS -->
    <link href="<?php //echo $fileDir; ?>css/reset.css" rel="stylesheet" type="text/css" />
    <link href="<?php //echo $fileDir; ?>css/columns.css" rel="stylesheet" type="text/css" />
    <link href="<?php //echo $fileDir; ?>css/parallax.css" rel="stylesheet" type="text/css" />
    <link href="<?php //echo $fileDir; ?>css/template.css" rel="stylesheet" type="text/css" />

    <!-- Javascript -->
    <script src="<?php //echo $fileDir ?>js/jquery-3.2.0.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="<?php //echo $fileDir ?>js/javascript.js" type="text/javascript" charset="utf-8"></script>

    <!-- Google analitics -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-98803799-1', 'auto');
        ga('send', 'pageview');

    </script>

    <!-- MarijnSlider -->
    <script src="js/marijnslider/marijnslider.js" type="text/javascript"></script>
    <link href="js/marijnslider/css/marijnslider.css" rel="stylesheet" type="text/css" />

</head>

<body>


<!-- Experimental parallax-->
<!--<div class="experimental-parallax hidden">-->
<!--    <p>Toggle parallax</p>-->
<!--</div>-->

<!-- Nav -->
<div class="nav-trigger">
    <span class="line-1"></span>
    <span class="line-2"></span>
    <span class="line-3"></span>
</div>

<nav class="main-nav">
    <ul>
        <li><a href="#home" class="to-home"><div class="logo mask"><img src="images/marijn_kleuskens_logo.svg" alt="Logo Marijn Kleuskens" /></div></a></li>
        <li><a href="#rubrics" class="ro-rubrics">S6 Rubrics</a></li>
        <li><a href="#highlighted-cases" class="to-showcases">Mijn werk</a></li>
        <li><a href="#about-contact" class="to-about-contact">Over mij</a></li>
    </ul>
</nav>

<div class="parallax-container">

    <!-- Intro -->
    <div class="parallax-group fullheight">

        <div class="parallax-layer parallax-back">
            <div class="container">

                <section class="home" id="home">

                    <article class="intro">
                        <div class="intro-title">
                            <figure class="logo">
                                <img src="images/marijn_kleuskens_logo_white-bgr.svg" alt="Logo Marijn Kleuskens" />
                            </figure>
                            <h1>Marijn Kleuskens</h1>
                        </div>
                        <div class="intro-subtitle">
                            <h2 class="white">Experience designer</h2>
                        </div>
                    </article>

                </section>

            </div>

        </div>

        <div class="parallax-layer parallax-deep">
            <figure class="intro-background-image"></figure>
        </div>

    </div>

    <div class="parallax-group site-main">
        <div class="parallax-base">
            <?php include 'inc/content.php'; ?>
            
            <div class="devider"></div>
            

            <section class="highlighted-cases" id="highlighted-cases">

                <div class="container">
                    <h2 class="black">Mijn werk</h2>

                    <?php
                    $showcaseCounter = 0;
                    foreach ($highlightedCases as $item){ ?>

                        <div class="highlighted-case-outer" id="<?php echo strtolower($item["title"]); ?>">
                            <article class="highlighted-case <?php echo $showcaseCounter; ?>" style="background-image: url('images/projects/<?php echo $item["background"]; ?>');">
                                <div class="highlighted-case-aspect-ratio">

                                    <section class="text-left">
                                        <div class="title mask">
                                            <h3 class="animate show"><?php echo $item["title"]; ?></h3>
                                        </div>
                                    </section>

                                </div>

                            </article>
                        </div>
                        <?php
                        $showcaseCounter++;
                    } ?>

                </div>

            </section>

            <?php if(isset($allCases)){ // It's not the school site ?>
                 

                <!-- All Cases -->

        <!--        <div class="parallax-group">-->
        <!---->
        <!--            <div class="parallax-layer parallax-base">-->
        <!---->
        <!--                <section class="all-cases" id="all-cases">-->
        <!---->
        <!--                    <div class="container">-->
        <!---->
        <!--                        <h2>Mijn werk</h2>-->
        <!---->
        <!--                        <div class="row">-->
        <!--                            --><?php
        //                            $caseCounter = 0;
        //                            foreach ($allCases as $item){ ?>
        <!--                                <div class="col-4">-->
        <!--                                    <article class="case --><?php //echo $caseCounter; ?><!--">-->
        <!--                                        <figure class="image mask">-->
        <!--                                            <img src="images/projects/--><?php //echo $item["first-image"]; ?><!--" alt="--><?php //echo $item["first-image"]; ?><!--" />-->
        <!--                                        </figure>-->
        <!---->
        <!--                                        <section class="titles">-->
        <!--                                            <h4>--><?php //echo $item["title"]; ?><!--</h4>-->
        <!--                                        </section>-->
        <!--                                        <div class="all-cases-active"></div>-->
        <!--                                    </article>-->
        <!--                                </div>-->
        <!---->
        <!--                                --><?php
        //                                $caseCounter++;
        //                            } ?>
        <!---->
        <!--                        </div>-->
        <!---->
        <!--                    </div>-->
        <!---->
        <!--                </section>-->
        <!---->
        <!--            </div> <!-- /.parallax-base -->
        <!---->
        <!--        </div> <!-- /.parallax-group -->
            <?php } ?>

            <section class="about-contact" id="about-contact">

                <div class="container">

                    <article class="about">
                        <h2 class="white">Over mij</h2>
                        <p>Mijn naam is Marijn en ik houd mij bezig met het ontwerpen en bouwen van websites, apps en digitale ervaringen. Ik specialiseer me in user experience design, user interface design, concepting en marketing. Ik houd er van om problemen op een creatieve en effectieve manier op te lossen.<br>
                            <br>
                            <br>
                            Ik ben een 24 jarige ICT & Media student aan het Fontys in Eindhoven. Hiervoor heb ik de opleiding Interactieve Vormgeving aan het Sint Lucas in Eindhoven gedaan.<br>
                            <br>
                            Ik heb bij drie bedrijven stage gelopen, namelijk <a target="_blank" href="http://www.appcomm.nl/">Studio Lupa (nu AppComm)</a>, <a target="_blank" href="http://www.mindworkz.nl/">Mindworkz</a> en <a target="_blank" href="http://www.thirdfloordesign.nl">Third Floor Design</a>.<br>
                        </p>
                    </article>

                    <article class="contact" id="contact">
                        <h2 class="white">Neem contact op</h2>
                        <form action="inc/email.php" method="post" name="send-email" class="send-email">
                            <label for="name"><span class="bold">Naam</span></label>
                            <input type="text" name="name" id="name" class="name contact-form-input" required />

                            <label for="email"><span class="bold">E-mail</span></label>
                            <input type="email" name="email" id="email" class="email contact-form-input" required />

                            <label for="message"><span class="bold">Bericht</span></label>
                            <textarea name="message" id="message" class="message contact-form-input" required></textarea>

                            <div class="button-container">
                                <button type="button" class="form-submit"><span class="main">verstuur</span><span class="d">d</span></button>
                            </div>

                        </form>
                    </article>

                </div>

            </section>

            <footer class="footer">
                <div class="light-blue"></div>
    <!--                    <div class="container">-->
    <!--                        <p>&copy; Marijn Kleuskens, --><?php //echo date("Y"); ?><!--</p>-->
    <!--                    </div>-->
            </footer>

        </div> <!-- /.parallax-group -->

    </div> <!-- /.parallax-container -->
</div>

<section class="content-box hidden">

    <div class="content-box-header">
        <div class="title mask">
            <h5 class="animate-below"></h5>
        </div>
        <div class="back">
            <span class="line-1"></span>
            <span class="line-2"></span>
        </div>
    </div>

    <section class="content-box-inner hidden animate-below-50">
        <div class="parallax-container height-of-parent">

            <div class="parallax-group-wrapper height-of-parent">
                <div class="parallax-group height-of-parent">

                    <div class="parallax-layer height-of-parent parallax-base">
                        <div class="content-box-intro-text">
                            <div class="text hidden animate-below-50">
                                <p><span class="italic"></span></p>
                            </div>
                            <div class="external-link"></div>
                        </div>
                        <figure class="arrow-down text hidden animate-below-50">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="0 0 595.3 297.6" style="enable-background:new 0 0 595.3 297.6;" xml:space="preserve">
                                    <polygon class="st0" points="297.6,297.6 0,0 5.8,0 297.6,291.9 589.5,0 595.3,0 "/>
                                </svg>
                        </figure>
                    </div>

                    <div class="parallax-layer height-of-parent parallax-deep">
                        <div class="content-box-intro-background"></div>
                    </div>

                </div>
            </div>

            <div class="parallax-group-wrapper min-height-of-parent content-box-chapter">
                <div class="parallax-group min-height-of-parent">

                    <div class="parallax-layer height-of-parent parallax-base">
                        <div class="left">
                            <div class="text"></div>
                        </div>
<!--                    </div>-->

<!--                    <div class="parallax-layer height-of-parent parallax-back">-->
                        <div class="right">
                            <div class="inner"></div>
                        </div>
                    </div>

                </div>
            </div>


        </div>

    </section>

</section>

</body>
</html>