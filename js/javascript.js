/**
 * Created by Marijn on 26-4-2016.
 */

$(document).ready(function(){

    $(".experimental-parallax").on("click", function () {
        $(".content-box-inner .parallax-group-wrapper").toggleClass("perspective-none");
    });

    //
    // ********************************************************************************************************************************** Navigation
    //

    $(".nav-trigger").on("click", function(){
        $(this).toggleClass("active");
        $(".main-nav").toggleClass("active");
    });

    var mainNavA = $(".main-nav a");
    mainNavA.on("click", function(){
        $(".nav-trigger, .main-nav").toggleClass("active");
    });

    //
    // ********************************************************************************************************************************** Intropage
    //



    //
    // ********************************************************************************************************************************** Scrolling
    //

    // On Scroll

    // var highlightedCases = $(".parallax-group-showcases");
    // var allCases         = $(".parallax-group-all-cases");
    // var aboutContact     = $(".parallax-group-about-contact");
    //
    // $(".parallax-container").on("scroll", function(event){
    //
    //     var $this = $(this);
    //
    //     console.log(highlightedCases.offset().top);
    //     console.log(allCases.offset().top);
    //     console.log(aboutContact.offset().top);
    //
    //     /* Navigation active classes */
    //     if(highlightedCases.offset().top <= ($this.scrollTop() + 200) && highlightedCases.offset().top + highlightedCases.height() > ($this.scrollTop() + 200)){
    //         mainNavA.removeClass("active");
    //         $(".main-nav a.to-showcases").addClass("active");
    //         // console.log("Showcases");
    //     } else if(allCases.offset().top <= ($this.scrollTop() + 200) && allCases.offset().top + (allCases.height() > $this.scrollTop() + 200)){
    //         mainNavA.removeClass("active");
    //         $(".main-nav a.to-all-cases").addClass("active");
    //         // console.log("all cases");
    //     } else if(aboutContact.offset().top <= ($this.scrollTop() + 200) && aboutContact.offset().top + (aboutContact.height() > $this.scrollTop() + 200)){
    //         mainNavA.removeClass("active");
    //         $(".main-nav a.to-about-contact").addClass("active");
    //         // console.log("About & contact");
    //     } else {
    //         mainNavA.removeClass("active");
    //         // console.log("Top");
    //     }
    //
    // });

    //
    // ********************************************************************************************************************************** Highlighted cases
    //

    // Show infobox, calls functions below
    $(".highlighted-cases .info-button a, .highlighted-case").on("click", function(){
        var clickedCase = $(this).closest(".highlighted-case").attr("class").split(" ")[1];
        showContentBox(clickedCase, "highlightedCases");
    });

    // Show titles and buttons
    function showTitlesAndButtons(){

        $(".highlighted-case .title h3").addClass("show");

        setTimeout(function () {
            $(".highlighted-case .info-button a.animate").addClass("show");
        }, 400);

        setTimeout(function () {
            $(".highlighted-case .visit-site-button a.animate").addClass("show");
        }, 500);

    }

    // Hide titles & buttons. Same as showTitlesAndButtons, but the other way around and quicker.
    function hideTitlesAndButtons(clickedCase){

        setTimeout(function () {
            $(".highlighted-case:eq("+clickedCase+") .title h3").removeClass("show");
        }, 400);

        setTimeout(function () {
            $(".highlighted-case:eq("+clickedCase+") .info-button a").removeClass("show");
        }, 200);

        $(".highlighted-case:eq("+clickedCase+") .visit-site-button a").removeClass("show");
    }

    //
    // ********************************************************************************************************************************** All Cases
    //

    // Show content box
    $(".all-cases a.info-button, .all-cases .case .image").on("click", function() {
        var clickedCase = $(this).closest(".case").attr("class").split(" ")[1];
        showContentBox(clickedCase, "allCases");
    });

    //
    // ********************************************************************************************************************************** Content box
    //


    function showContentBox(clickedCase, arraySelect){

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "inc/content.php",
            // url: "inc/content-school.php",
            data: {
                index: clickedCase,
                arraySelect: arraySelect
            },
            success: function(content){

                var delay = 0;

                if(arraySelect == "highlightedCases"){
                    hideTitlesAndButtons(clickedCase);
                    delay = 500;
                }

                // Append the content
                // Titles
                $(".content-box-header .title h5").html(content.title);
                // Link to external site if necessary
                var externallink = "";
                if(content.externallink.url !== ""){
                    externallink = "<a href ='"+content.externallink.url+"' target='_blank' class='hidden animate-below-50'>"+content.externallink.urltext+"</a>";
                }
                $(".content-box-inner .external-link").html(externallink);

                // Fill the content box with content
                var chapter = $(".content-box-chapter");

                // Intro
                var introChapter = chapter.clone();
                var introImagePath = "images/projects/"+content.background;
                $(".content-box-intro-background").css("background-image", "url("+introImagePath+")");
                $(".content-box-intro-text p span").html(content.content.intro);

                $.each(content.content.chapters, function(key, value){

                    var newChapter = chapter.clone();
                    newChapter.addClass("new-chapter");

                    // Text
                    var title = "<span class='bold'>"+key+"</span>";
                    newChapter.find(".text").append(title);
                    $.each(value["text"], function(k, v) {
                        var node = "<p>"+v+"</p>";
                        newChapter.find(".text").append(node);
                    });

                    // Right
                    var right = "";
                    $.each(value["right"], function(k, v){
                        if(v[0] === "audio"){
                            if(v[1].length === 2) {
                                right += "<div class='right-wrapper audio-wrapper'>" +
                                    "<p>"+v[1][0]+"</p>"+
                                    "<audio controls class='audiofile'>"+
                                    "<source src='files/"+ v[1][1] +"'>"+
                                    "</audio>"+
                                    "</div>";
                            } else {
                                right += "<div class='right-wrapper audio-wrapper'>" +
                                    "<audio controls class='audiofile'>"+
                                    "<source src='files/"+v[1]+"'>"+
                                    "</audio>"+
                                    "</div>";
                            }
                        } else if (v[0] === "image"){
                            var path = "images/projects/"+v[1];
                            right += "<div class='right-wrapper img-wrapper'>" +
                                    "<div class='content-box-image' style='background-image: url("+path+")'/>"+
                                "</div>";
                        } else if (v[0] === "youtube"){
                            right += "<div class='right-wrapper yt-wrapper'>"+v[1]+"</div>";
                        } else if (v[0] === "video"){
                            right += "<div class='right-wrapper video-wrapper'>" +
                                "<video width='100%' controls class='video'>" +
                                "<source src='files/"+v[1]+"' type='video/mp4'>"+
                                "</video>"+
                                "</div>";
                        }  else if (v[0] === "marijnslider"){
                            right += v[1];
                        }
                    });
                    newChapter.find(".right .inner").html(right);
                    // Call MarijnSlider
                    $(".marijn-slider").marijnSlider({
                        autoSlide: 5000,
                        arrowPosition: "inside"
                    });

                    chapter.parent().append(newChapter);
                });
                $(".content-box-chapter:first").hide();

                // Hide the nav bar
                $(".nav-trigger").addClass("hidden");

                // Scroll to the top of the content box so that when it's opened it always starts at the top
                $(".content-box .content-box-inner .parallax-container").scrollTop(0);

                setTimeout(function(){
                    // Show the outer and inner div
                    $(".content-box, .content-box-inner, .experimental-parallax").removeClass("hidden").addClass("show");
                }, delay);

                // Disable body scrolling. This is called after the outer and inner div have been rendered so that they won't skip about when the scroll bar disappears.
                setTimeout(function(){
                    $("body").css("overflow", "hidden");
                }, (delay + 500));

                // Show the titles and content of the infobox
                setTimeout(function(){
                    $(".content-box-header .title h5").addClass("show");
                }, (delay + 500));
                setTimeout(function(){
                    $(".content-box-inner .content-box-intro-text .text").removeClass("hidden").addClass("show");
                }, (delay + 750));
                if(content.externallink.url != ""){
                    setTimeout(function(){
                        $(".content-box-inner .external-link a").addClass("show");
                    }, (delay + 1000));
                    delay += 250;
                }
                setTimeout(function(){
                    $(".content-box-inner .arrow-down").removeClass("hidden").addClass("show");
                }, (delay + 1250));

            }

        });

    }

    // Scroll down content box
    $(".content-box .arrow-down").on("click", function () {
        var $this = $(this);
        $this.closest(".parallax-container").animate({
            scrollTop: 300
        }, 0);
    });

    // Hide the content box
    $(".content-box .back").on("click", function(){

        // ---------------------- Pretty version

        // // Hide the back-button
        // $(".content-box .back img").removeClass("show");
        //
        // // Hide the titles and content of the infobox
        // setTimeout(function(){
        //     $(".content-box-header .title h5").removeClass("show");
        // }, 250);
        // setTimeout(function(){
        //     $(".content-box-inner .images").addClass("hidden").removeClass("show");
        // }, 750);
        // setTimeout(function(){
        //     $(".content-box-inner .text").addClass("hidden").removeClass("show");
        // }, 850);
        // setTimeout(function(){
        //     $(".content-box-inner .external-link a").addClass("hidden").removeClass("show");
        // }, 950);
        //
        // // Hide the content-box itself
        // setTimeout(function(){
        //     $(".content-box, .content-box-inner").addClass("hidden").removeClass("show");
        // }, 1100);
        //
        // // Enable body scrolling, show highlighted cases titles and buttons, show the nav bar
        // setTimeout(function(){
        //     $("body").css("overflow", "auto");
        //     $(".nav-trigger").removeClass("hidden");
        //     showTitlesAndButtons();
        // }, 1100);

        // -------------------------- Quick version

        // Hide the back-button
        $(".content-box .back img").removeClass("show");

        // Pause any video
        var video = $(".content-box-inner .content .right .inner .video");
        if(video.length !== 0){
            video.get(0).pause();
        }
        // Pause any audio
        var audio = $(".content-box-inner .content .right .inner .audiofile");
        if(audio.length !== 0){
            audio.get(0).pause();
        }
        // Pause any youtube video
        var ytvideo = $(".content-box-inner .content .right .inner .yt-wrapper iframe");
        if(ytvideo.length !== 0){
            $(".content-box-inner .content .right .inner .yt-wrapper").html("");
        }

        // Hide the titles and content of the infobox
        $(".content-box-header .title h5").removeClass("show");
        $(".content-box-inner .content-box-intro-text .text").addClass("hidden") .removeClass("show");
        $(".content-box-inner .arrow-down").addClass("hidden") .removeClass("show");
        $(".content-box-inner .content .external-link a").addClass("hidden").removeClass("show");
        $(".experimental-parallax").addClass("hidden").removeClass("show");

        // Hide the content-box itself
        $(".content-box, .content-box-inner").addClass("hidden").removeClass("show");

        // Remove the content
        $(".new-chapter").remove();
        $(".content-box-chapter:first").show();

        // Enable body scrolling, show highlighted cases titles and buttons, show the nav bar
        $("body").css("overflow", "auto");
        $(".nav-trigger").removeClass("hidden");
        showTitlesAndButtons();

    });

    //
    // ********************************************************************************************************************************** Form
    //

    // Validate name
    $(".name").on("blur", function(){

        var $this = $(this);

        if($this.val()){
            if(checkName($this.val())){
                if(!$this.hasClass("true")){
                    $this.removeClass("false").addClass("true");
                }
            } else {
                if(!$this.hasClass("false")){
                    $this.removeClass("true").addClass("false");
                }
            }

            function checkName(name) {
                var re = /^[A-Za-z .'-]+$/;
                return re.test(name); // true or false
            }
        } else {
            $this.removeClass("false");
        }

    });

    // Validate e-mail
    $(".email").on("blur", function(){

        var $this = $(this);

        if($this.val()){
            if(checkEmail($this.val())){
                if(!$this.hasClass("true")){
                    $this.removeClass("false").addClass("true");
                }
            } else {
                if(!$this.hasClass("false")){
                    $this.removeClass("true").addClass("false");
                }
            }

            function checkEmail(email) {
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email); // true or false
            }
        } else {
            $this.removeClass("false");
        }

    });

    // Validate message
    $(".message").on("blur", function(){

        var $this = $(this);

        if($this.val()) {
            if (!$this.hasClass("true")) {
                $this.removeClass("false").addClass("true");
            }
        } else {
            $this.removeClass("false, true");
        }
    });

    // Submit form
    $(".form-submit").on("click", function () {

        // See if any inputs are false or not filled in.
        var emptyImputs = $(".contact-form-input").not(".true");

        // If there are some inputs false or not filled in bring attention to them. Else submit form
        if(emptyImputs.length) {
            $.each(emptyImputs, function(){

                var $this = $(this);

                // Play animation
                $this.addClass("no-submit");

                // Clone this and put it before this, so that the animation can be called on the clone later
                $this.before($this.clone(true));

                // Remove this
                $this.remove();
            });
        } else {
            var formInputs = {};

            $("form.send-email .contact-form-input").each(function () {
                var $this = $(this);
                formInputs[$this.attr("name")] = $this.val();
            });

            // console.log(formInputs);

            $.ajax({
                type: "POST",
                dataType: "json",
                url: "inc/email.php",
                data: {action: formInputs},
                success: function(response){
                    // console.log(response);

                    if(response.success){
                        // success
                        $(".form-submit")
                            .addClass("success")
                            .prop('disabled', true);

                        $(".contact-form-input").prop('disabled', true);
                    }
                }
            });

            // $(".send-email").submit();
        }

    });

});